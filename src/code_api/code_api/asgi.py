import os

from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
from django.core.asgi import get_asgi_application

import demo.websocket.routing

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'code_api.settings')

application = ProtocolTypeRouter({
    "http": get_asgi_application(),
    "websocket": AuthMiddlewareStack(
        URLRouter(
            demo.websocket.routing.websocket_urlpatterns
        )
    ),
})
