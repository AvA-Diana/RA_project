from asgiref.sync import async_to_sync
from celery import shared_task
from channels.layers import get_channel_layer

from demo.models import VerificationCode, OrderDish, Order
from demo.websocket.ChannelGroupName import ChannelGroupNameEnum


@shared_task
def clean_verification_code(verification_code_id):
    target_verification_code = VerificationCode.objects.get(id=verification_code_id)
    target_verification_code.delete()


@shared_task
def send_order2cook():
    unfinished_dishes = []
    for order_dish in OrderDish.objects.exclude(undone_count=0).all():
        dish = order_dish.dish
        unfinished_dishes.append({
            'id': order_dish.id,
            'dish': dish.name,
            'count': order_dish.undone_count,
            'image': dish.image.url,
        })
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)(
        ChannelGroupNameEnum.COOK_GROUP_NAME.value,
        {
            'type': 'cook_order',
            'order': unfinished_dishes
        }
    )


@shared_task
def send_finished_order2waiter():
    finished_dishes = []
    for order_dish in OrderDish.objects.exclude(done_count=0).all():
        dish = order_dish.dish
        finished_dishes.append({
            'id': order_dish.id,
            'dish': dish.name,
            'count': order_dish.done_count,
            'image': dish.image.url,
            'table': order_dish.order.table.number,
        })
    finished_dishes.reverse()
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)(
        ChannelGroupNameEnum.WAITER_GROUP_NAME.value,
        {
            'type': 'waiter_order',
            'order': finished_dishes
        }
    )
