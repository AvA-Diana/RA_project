from django.contrib import admin

from demo.models import Account, Dish, Category, OrderDish, Order, VerificationCode
from demo.models import Table

admin.site.register(Account)
admin.site.register(Table)
admin.site.register(Dish)
admin.site.register(Category)
admin.site.register(Order)
admin.site.register(OrderDish)
admin.site.register(VerificationCode)
