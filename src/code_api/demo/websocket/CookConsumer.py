import json
from channels.db import database_sync_to_async
from channels.generic.websocket import WebsocketConsumer, AsyncWebsocketConsumer

from demo.models import Account
from demo.celery_task import send_order2cook
from demo.websocket.ChannelGroupName import ChannelGroupNameEnum


def send_order():
    send_order2cook.apply_async()


class CookConsumer(AsyncWebsocketConsumer):

    def __init__(self, *args, **kwargs):
        super().__init__(args, kwargs)
        self.cook_group_name = ChannelGroupNameEnum.COOK_GROUP_NAME.value
        self.option_dict = {
            "get_order": send_order
        }

    async def connect(self):
        # await self.accept()
        # await self.channel_layer.group_add(
        #     self.cook_group_name,
        #     self.channel_name
        # )
        user = self.scope['user']
        if user.is_anonymous:
            await self.close()
        elif await self.is_cook(user):
            await self.channel_layer.group_add(
                self.cook_group_name,
                self.channel_name
            )
            await self.accept()
        else:
            await self.close()

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(
            self.cook_group_name,
            self.channel_name
        )

    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        option = text_data_json['option']
        target_option = self.option_dict.get(option)
        if target_option:
            target_option()
        else:
            await self.send(text_data="pong!")

    async def cook_order(self, event):
        order = event['order']
        await self.send(text_data=json.dumps(order))

    @database_sync_to_async
    def is_cook(self, user):
        return True if Account.objects.get(user=user).role == Account.COOK else False
