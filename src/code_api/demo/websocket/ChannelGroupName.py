from enum import Enum


class ChannelGroupNameEnum(Enum):
    COOK_GROUP_NAME = "CookGroup"
    WAITER_GROUP_NAME = "WaiterGroup"
