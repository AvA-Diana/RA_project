import json

from channels.generic.websocket import AsyncWebsocketConsumer
from demo.celery_task import send_finished_order2waiter
from demo.websocket.ChannelGroupName import ChannelGroupNameEnum


def send_finished_order():
    send_finished_order2waiter.apply_async()


class WaiterConsumer(AsyncWebsocketConsumer):
    def __init__(self, *args, **kwargs):
        super().__init__(args, kwargs)
        self.customer_group_name = ChannelGroupNameEnum.WAITER_GROUP_NAME.value
        self.option_dict = {
            "get_finished_order": send_finished_order
        }

    async def connect(self):
        # await self.channel_layer.group_add(
        #     self.customer_group_name,
        #     self.channel_name
        # )
        # await self.accept()

        user = self.scope['user']
        if user.is_anonymous:
            await self.close()
        else:
            await self.channel_layer.group_add(
                self.customer_group_name,
                self.channel_name
            )
            await self.accept()

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(
            self.customer_group_name,
            self.channel_name
        )

    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        option = text_data_json['option']
        target_option = self.option_dict.get(option)
        if target_option:
            target_option()
        else:
            await self.send(text_data="pong!")

    async def waiter_order(self, event):
        order = event['order']
        await self.send(text_data=json.dumps(order))
