from django.urls import re_path

from . import CookConsumer
from . import WaiterConsumer

websocket_urlpatterns = [
    re_path(r'cook/$', CookConsumer.CookConsumer.as_asgi()),
    re_path(r'waiter/$', WaiterConsumer.WaiterConsumer.as_asgi()),
]
