import os
import random
import smtplib
import string
from urllib import parse

from django.contrib.auth import authenticate, logout, login
from django.contrib.auth.models import User, Permission
from django.core.mail import send_mail

from demo.models import Account, Table, Order, OrderDish

# 为账户添加权限
from demo.serializers import DishSerializer


def add_permission(target_account):
    permission_list = []
    for needed_permission in ['account', 'dish', 'order', 'table', 'orderdish', 'category', 'verificationcode']:
        permission_list += Permission.objects.filter(codename__contains=needed_permission).all()
    for permission in permission_list:
        target_account.user.user_permissions.add(permission)


# 获取身份
def get_role(role):
    role_type = {
        'manager': Account.MANAGER,
        'cook': Account.COOK,
        'busboy': Account.BUSBOY,
        'waiter': Account.WAITER,
        'receptionist': Account.RECEPTIONIST
    }
    return role_type[role]


def get_role4chinese(role):
    role_type = {
        Account.MANAGER: '经理',
        Account.COOK: '厨师',
        Account.BUSBOY: '清洁工',
        Account.WAITER: '服务员',
        Account.RECEPTIONIST: '前台'
    }
    return role_type[role]


def get_sex4chinese(sex):
    sex_type = {Account.MALE: '男', Account.FEMALE: '女'}
    return sex_type[sex]


# 用户注册
def user_register(request):
    username = request.data.get('email')
    password = request.data.get('password')
    email = request.data.get('email')
    new_user = User.objects.create_user(username=username, password=password, email=email)
    return new_user


# 账户登录函数
def account_login(request):
    username = request.data.get('username')
    password = request.data.get('password')
    user = authenticate(username=username, password=password)
    if user:
        login(request, user)
        return True
    else:
        return False


# 账户登出函数
def account_logout(request):
    logout(request)
    request.session.clear()


def get_account_info(target_account):
    target_user = target_account.user
    data = {
        'id': target_account.id,
        'name': target_account.name,
        'username': target_user.username,
        'email': target_user.email,
        'role': get_role4chinese(target_account.role),
        'sex': get_sex4chinese(target_account.sex),
        'avatar': target_account.avatar.url if target_account.avatar else '',
        'phone': target_account.phone
    }
    return data


def set_user_info(target_user, request):
    target_user.username = request.data.get('email')
    target_user.email = request.data.get('email')
    target_user.save()


# 创建订单
def create_order(request):
    target_table = Table.objects.get(number=request.data.get('table'))
    start_time = request.data.get('start_time')
    people_count = request.data.get('people_count')
    new_order = Order.objects.create(table=target_table, start_time=start_time, people_count=people_count)
    new_order.save()
    return new_order


# 结束订单
def finish_order(request):
    target_table = Table.objects.get(number=request.data.get('table'))
    target_order = Order.objects.get(table=target_table, state=Order.UNPAID)
    target_order.state = Order.PAID
    target_order.due_time = request.data.get('due_time')
    target_order.save()
    return target_order


# 删除文件
def delete_file(path):
    if os.path.exists:
        os.remove(parse.unquote(path))
    else:
        raise FileNotFoundError


def send_email(user, subject, email_message):
    try:
        target_email = user.email
        send_mail(from_email=None,
                  recipient_list=[target_email],
                  fail_silently=False,
                  subject=subject,
                  message=email_message,
                  )
        return True
    except smtplib.SMTPException as e:
        return False


def generate_verification_code():
    capta = ''
    words = ''.join((string.ascii_letters, string.digits))
    for i in range(6):
        capta += random.choice(words)
    return capta
