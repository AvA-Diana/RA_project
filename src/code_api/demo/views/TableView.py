from django.db import transaction
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from demo.models import Table, Account
from demo.serializers import TableSerializer
from demo.views.public_func import create_order, finish_order


class TableView(viewsets.ModelViewSet):
    queryset = Table.objects.all()
    serializer_class = TableSerializer

    @action(methods=['post'], detail=False)
    def add_table(self, request):
        try:
            new_table = Table.objects.create(number=request.data.get('number'))
            new_table.save()
            return Response('succeed')
        except Exception as e:
            return Response('failed')

    @action(methods=['get'], detail=False)
    def get_tables(self, request):
        try:
            table = Table.objects.all()
            serializer = TableSerializer(table, many=True)
            return Response(serializer.data)
        except Exception:
            return Response('failed')

    @action(methods=['post'], detail=False)
    def delete_table_by_number(self, request):
        try:
            table = Table.objects.get(number=request.data.get('number'))
            table.delete()
            return Response('succeed')
        except Exception:
            return Response('failed')

    @action(methods=['post'], detail=True)
    def delete_table(self, request, pk):
        try:
            table = Table.objects.get(id=pk)
            table.delete()
            return Response('succeed')
        except Exception:
            return Response('failed')

    @action(methods=['post'], detail=True)
    def move_table(self, request, pk):
        try:
            table = Table.objects.get(id=pk)
            table.x = request.data.get('x')
            table.y = request.data.get('y')
            table.save()
            return Response('succeed')
        except Exception:
            return Response('failed')

    @action(methods=['get'], detail=False)
    def get_available_table(self, request):
        try:
            target_account = Account.objects.get(user=request.user)
            available_tables = ''
            if target_account.role == Account.MANAGER:
                available_tables = Table.objects.filter(state=Table.AVAILABLE).all()
            elif target_account.role == Account.WAITER:
                available_tables = Table.objects.filter(waiter=target_account, state=Table.AVAILABLE).all()
            else:
                return Response('failed')
            for table in available_tables:
                pass
        except Exception as e:
            return Response(str(e))

    @action(methods=['post'], detail=False)
    def occupy_table(self, request):
        try:
            with transaction.atomic():
                create_order(request)
                target_table = Table.objects.get(number=request.data.get('table'))
                target_table.state = Table.IN_USE
                target_table.save()
                return Response('succeed')
        except Exception as e:
            return Response(str(e))

    @action(methods=['post'], detail=False)
    def release_table(self, request):
        try:
            with transaction.atomic():
                finish_order(request)
                target_table = Table.objects.get(number=request.data.get('table'))
                target_table.state = Table.TO_BE_CLEANED
                target_table.save()
                return Response('succeed')
        except Exception as e:
            return Response(str(e))

    @action(methods=['post'], detail=False)
    def clean_table(self, request):
        try:
            target_table = Table.objects.get(number=request.data.get('table'))
            target_table.state = Table.AVAILABLE
            target_table.save()
            return Response('succeed')
        except Exception as e:
            return Response(str(e))
