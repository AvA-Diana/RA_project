from django.db import transaction
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from demo.models import Category, Dish
from demo.serializers import CategorySerializer, DishSerializer


class CategoryView(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    @action(methods=['post'], detail=False)
    def add_category(self, request):
        try:
            category_name = request.data.get('category_name')
            new_category = Category.objects.create(category_name=category_name)
            new_category.save()
            return Response('succeed')
        except Exception as e:
            return Response('failed')

    @action(methods=['post'], detail=False)
    def modify_categories(self, request):
        try:
            with transaction.atomic():
                categories = request.data.get('categories')
                for category in categories:
                    if category['id'] == -1:
                        new_category = Category.objects.create(category_name=category['category_name'])
                        new_category.save()
                        continue
                    modified_category = Category.objects.get(id=category['id'])
                    modified_category.category_name = category['category_name']
                    modified_category.save()
                delete_categories = request.data.get('delete_categories')
                for delete_category in delete_categories:
                    if delete_category['id'] == -1:
                        continue
                    deleted_category = Category.objects.get(id=delete_category['id'])
                    deleted_category.delete()
                return Response('succeed')
        except Exception as e:
            return Response(str(e))

    @action(methods=['post'], detail=True)
    def delete_category(self, request, pk):
        try:
            target_category = Category.objects.get(id=pk)
            target_category.delete()
            return Response('succeed')
        except Exception as e:
            return Response('failed')

    @action(methods=['get'], detail=False)
    def get_categories(self, request):
        try:
            categories = Category.objects.all().order_by('id')
            return Response(CategorySerializer(categories, many=True).data)
        except Exception as e:
            return Response('failed')

    @action(methods=['post'], detail=False)
    def get_dishes_by_category(self, request):
        try:
            target_category = Category.objects.get(category_name=request.data.get('category_name'))
            dishes = Dish.objects.filter(category=target_category).all()
            return Response(DishSerializer(dishes, many=True).data)
        except Exception as e:
            return Response('failed')
