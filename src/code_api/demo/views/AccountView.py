from django.contrib.auth import login
from django.db import transaction
from pymysql import IntegrityError
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from demo.models import Account
from demo.serializers import AccountSerializer
from demo.views.public_func import account_logout, get_role, get_role4chinese, get_sex4chinese, delete_file, \
    get_account_info, set_user_info


class AccountView(viewsets.ModelViewSet):
    queryset = Account.objects.all()
    serializer_class = AccountSerializer

    @action(methods=['post'], detail=False)
    def logout(self, request):
        try:
            account_logout(request)
            if request.user.is_anonymous:
                return Response('succeed')
            else:
                return Response('failed')
        except Exception as e:
            return Response('failed')

    @action(methods=['post'], detail=False)
    def modify_password(self, request):
        try:
            target_user = request.user
            if target_user.check_password(request.data.get('old_password')):
                target_user.set_password(request.data.get('new_password'))
                target_user.save()
                login(request, target_user)
                return Response('succeed')
            else:
                return Response('wrong password')
        except Exception as e:
            return Response(str(e))

    @action(methods=['get'], detail=False)
    def get_identity(self, request):
        try:
            target_account = Account.objects.get(user=request.user)
            return Response(str(target_account.get_role_display()))
        except Exception as e:
            return Response('failed')

    @action(methods=['post'], detail=False)
    def get_staff(self, request):
        try:
            data = []
            accounts = Account.objects.filter(role=get_role(request.data.get('role_type'))).all()
            for account in accounts:
                temp_account = {'name': account.name,
                                'avatar': account.avatar.url if account.avatar else None,
                                'id': account.id}
                data.append(temp_account)
            return Response(data)
        except Exception as e:
            return Response('failed')

    @action(methods=['get'], detail=True)
    def get_account_info(self, request, pk):
        try:
            target_account = Account.objects.get(id=pk)
            data = get_account_info(target_account)
            return Response(data)
        except Exception as e:
            return Response('failed')

    @action(methods=['post'], detail=False)
    def modify_avatar(self, request):
        try:
            target_account = Account.objects.get(user=request.user)
            target_avatar = target_account.avatar
            if target_avatar:
                delete_file('.' + target_avatar.url)
            target_account.avatar = request.FILES.get('avatar')
            target_account.save()
            return Response('succeed')
        except Exception as e:
            return Response('failed')

    @action(methods=['post'], detail=True)
    def delete_account(self, request, pk):
        try:
            with transaction.atomic():
                target_account = Account.objects.get(id=pk)
                target_user = target_account.user
                if target_account:
                    if target_account.avatar:
                        delete_file('.' + target_account.avatar.url)
                    target_user.delete()
                return Response('succeed')
        except Exception as e:
            return Response(str(e))

    @action(methods=['post'], detail=True)
    def reset_account_info(self, request, pk):
        try:
            with transaction.atomic():
                target_account = Account.objects.get(id=pk)
                target_account.name = request.data.get('name')
                target_account.sex = Account.MALE if request.data.get('sex') == 'male' else Account.FEMALE
                target_account.phone = request.data.get('phone')
                set_user_info(target_account.user, request)
                target_account.save()
                return Response('succeed')
        except IntegrityError:
            return Response('Duplicate email')
        except Exception as e:
            return Response(str(e))

    @action(methods=['get'], detail=False)
    def get_login_account_info(self, request):
        try:
            target_account = Account.objects.get(user=request.user)
            return Response(get_account_info(target_account))
        except Exception as e:
            return Response('failed')
