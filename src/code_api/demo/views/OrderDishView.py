from django.db import transaction
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from demo.models import OrderDish, Dish, Table, Order
from demo.serializers import OrderDishSerializer, DishSerializer
from demo.celery_task import send_order2cook, send_finished_order2waiter


class OrderDishView(viewsets.ModelViewSet):
    queryset = OrderDish.objects.all()
    serializer_class = OrderDishSerializer

    @action(methods=['post'], detail=False)
    def inquiry_order(self, request):
        try:
            table = Table.objects.get(number=request.data.get('table'))
            target_order = Order.objects.filter(table=table, state=Order.UNPAID).all()
            if target_order.count() == 1:
                order_dishes = OrderDish.objects.filter(order=(target_order.first())).all()
                dishes = []
                done_dish_counts = []
                undone_dish_counts = []
                for order_dish in order_dishes:
                    dishes.append(order_dish.dish)
                    done_dish_counts.append(order_dish.done_count)
                    undone_dish_counts.append(order_dish.undone_count)
                return Response(list(zip(DishSerializer(dishes, many=True).data, done_dish_counts, undone_dish_counts)))
            else:
                return Response('multiple qualified orders')
        except Exception as e:
            return Response(str(e))

    @action(methods=['post'], detail=False)
    def take_order(self, request):
        try:
            with transaction.atomic():
                target_table = Table.objects.get(number=request.data.get('table'))
                target_orders = Order.objects.filter(table=target_table, state=Order.UNPAID).all()
                if target_orders.count() == 1:
                    target_order = target_orders.first()
                    target_order.total_price = target_order.total_price + float(request.data.get('sum'))
                    target_order.save()
                    dishes_id = request.data.get('dishes_id')
                    dish_counts = request.data.get('dish_counts')
                    for dish_id, dish_count in zip(dishes_id, dish_counts):
                        target_dish = Dish.objects.get(id=dish_id)
                        if OrderDish.objects.filter(dish=target_dish, order=target_order).all().count() == 0:
                            new_order_dish = OrderDish.objects.create(
                                dish=target_dish, order=target_order, undone_count=dish_count
                            )
                            new_order_dish.save()
                        else:
                            target_order_dish = OrderDish.objects.filter(
                                dish=target_dish, order=target_order
                            ).all().first()
                            target_order_dish.undone_count += dish_count
                            target_order_dish.save()
                    send_order2cook.apply_async()
                    return Response('succeed')
                else:
                    return Response('multiple qualified orders')
        except Exception as e:
            return Response(str(e))

    @action(methods=['post'], detail=True)
    def order_dishes_done(self, request, pk):
        try:
            target_order_dish = OrderDish.objects.get(id=pk)
            done_count = request.data.get('count')
            target_order_dish.undone_count -= done_count
            target_order_dish.done_count += done_count
            target_order_dish.save()
            send_order2cook.apply_async()
            send_finished_order2waiter.apply_async()
            return Response('succeed')
        except Exception as e:
            return Response(str(e))

    @action(methods=['post'], detail=True)
    def order_dishes_served(self, request, pk):
        try:
            target_order_dish = OrderDish.objects.get(id=pk)
            served_count = request.data.get('count')
            target_order_dish.done_count -= served_count
            target_order_dish.served_count += served_count
            target_order_dish.save()
            send_order2cook.apply_async()
            send_finished_order2waiter.apply_async()
            return Response('succeed')
        except Exception as e:
            return Response(str(e))
