from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.contrib.auth import get_user, login
from django.contrib.auth.models import User
from django.db import transaction
from pymysql import IntegrityError
from rest_framework import viewsets, permissions
from rest_framework.decorators import action
from rest_framework.response import Response

from demo.models import Account, VerificationCode, Table
from demo.serializers import AccountSerializer
from demo.celery_task import clean_verification_code, send_finished_order2waiter
from demo.views.public_func import user_register, account_login, add_permission, get_role, generate_verification_code, \
    send_email


class AccountAllowAnyView(viewsets.ModelViewSet):
    queryset = Account.objects.all()
    serializer_class = AccountSerializer
    permission_classes = [permissions.AllowAny]

    @action(methods=['post'], detail=False)
    def register(self, request):
        stage = 'creating User'
        try:
            with transaction.atomic():
                sex_type = request.data.get('sex_type')
                sex = Account.MALE if sex_type == 'male' else Account.FEMALE
                new_user = user_register(request)
                stage = 'creating account'
                new_account = Account.objects.create(user=new_user, role=get_role(request.data.get('role_type')),
                                                     sex=sex, phone=request.data.get('phone'),
                                                     name=request.data.get('name'))
                stage = 'add_permission'
                add_permission(new_account)
                new_account.save()
                return Response('succeed')
        except IntegrityError:
            return Response('Duplicate username')
        except Exception as e:
            return Response(stage + 'failed')

    @action(methods=['post'], detail=True)
    def login4miniapp(self, request, pk):
        try:
            target_table = Table.objects.get(number=request.data.get('table_number'))
            target_user = target_table.waiter.user
            login(request, target_user)
            if request.user.is_anonymous:
                return Response('failed')
            else:
                return Response('succeed')
        except Exception as e:
            return Response(str(e))

    @action(methods=['post'], detail=False)
    def login(self, request):
        try:
            if account_login(request):
                return Response('succeed')
            else:
                return Response('failed')
        except Exception as e:
            return Response('error')

    @action(methods=['get'], detail=False)
    def get_login_status(self, request):
        try:
            target_user = get_user(request)
            if target_user.is_anonymous:
                return Response('Not logged')
            else:
                return Response('Logged')
        except Exception as e:
            return Response('failed')

    @action(methods=['post'], detail=False)
    def send_verification_code(self, request):
        try:
            target_user = User.objects.get(username=request.data.get('username'))
            verification_code = generate_verification_code()
            dead_time = 5
            msg = '验证码为：\n %s \n\n\n %s 分钟内有效' % (verification_code, repr(dead_time))
            if send_email(
                    user=target_user,
                    subject='重置密码验证码',
                    email_message=msg
            ):
                new_verification_code = VerificationCode.objects.create(user=target_user,
                                                                        verification_code=verification_code)
                new_verification_code.save()
                clean_verification_code.apply_async((new_verification_code.id,), countdown=dead_time * 60, )
                return Response('succeed')
            return Response('failed')
        except Exception as e:
            return Response(str(e))

    @action(methods=['post'], detail=False)
    def retrieve_password(self, request):
        try:
            username = request.data.get('username')
            target_user = User.objects.get(username=username)
            verification_code = VerificationCode.objects.filter(
                user=target_user,
                verification_code=request.data.get('verification_code')
            ).all()
            password = request.data.get('password')
            with transaction.atomic():
                if verification_code.count() == 1:
                    target_user.set_password(password)
                    target_user.save()
                    return Response('succeed')
                else:
                    return Response('verification failure')
        except Exception as e:
            return Response(str(e))

    @action(methods=['post'], detail=False)
    def test_func(self, request):
        send_finished_order2waiter.apply_async()
        return Response('succeed')
