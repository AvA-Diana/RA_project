from rest_framework import viewsets
from rest_framework.decorators import action

from demo.models import Order
from demo.serializers import OrderSerializer


class OrderView(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer

