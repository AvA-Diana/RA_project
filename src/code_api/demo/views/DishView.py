from django.db import transaction
from rest_framework import viewsets, permissions
from rest_framework.decorators import action, permission_classes
from rest_framework.response import Response

from demo.models import Dish, Category
from demo.serializers import DishSerializer
from demo.views.public_func import delete_file


class DishView(viewsets.ModelViewSet):
    queryset = Dish.objects.all()
    serializer_class = DishSerializer

    @action(methods=['post'], detail=False)
    def add_dish(self, request):
        try:
            name = request.data.get('name')
            price = request.data.get('price')
            category = Category.objects.get(id=request.data.get('category'))
            new_dish = Dish.objects.create(name=name,  price=price, category=category, expected_time=1)
            new_dish.save()
            return Response('succeed')
        except Exception as e:
            print(e)
            return Response(str(e))

    @action(methods=['post'], detail=True)
    def delete_dish(self, request, pk):
        try:
            target_dish = Dish.objects.get(id=pk)
            if target_dish.image:
                delete_file('.' + target_dish.image.url)
            target_dish.delete()
            return Response('succeed')
        except Exception as e:
            return Response(str(e))

    @permission_classes(permissions.IsAuthenticated)
    @action(methods=['post'], detail=True)
    def modify_dish(self, request, pk):
        try:
            with transaction.atomic():
                target_dish = Dish.objects.get(id=pk)
                name = request.data.get('name')
                price = request.data.get('price')
                category = Category.objects.get(id=request.data.get('category'))
                if name:
                    target_dish.name = name
                if price:
                    target_dish.price = price
                if category:
                    target_dish.category = category
                target_dish.save()
            return Response('succeed')
        except Exception as e:
            return Response('failed')

    @action(methods=['get'], detail=False)
    def get_dishes(self, request):
        try:
            dishes = Dish.objects.all()
            return Response(DishSerializer(dishes, many=True).data)
        except Exception as e:
            return Response('failed')

    @action(methods=['post'], detail=True)
    def modify_dish_image(self, request, pk):
        try:
            target_dish = Dish.objects.get(id=pk)
            target_image = target_dish.image
            if target_image:
                delete_file('.' + target_image.url)
            target_dish.image = request.FILES.get('image')
            target_dish.save()
            return Response('succeed')
        except Exception as e:
            return Response('failed')
