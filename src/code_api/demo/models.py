from django.contrib.auth.models import User
from django.db import models


class Account(models.Model):
    user = models.OneToOneField(to=User, on_delete=models.CASCADE, null=True)
    MANAGER = 0
    COOK = 1
    BUSBOY = 2
    WAITER = 3
    RECEPTIONIST = 4
    role_type = [
        (MANAGER, 'manager'),
        (COOK, 'cook'),
        (BUSBOY, 'busboy'),
        (WAITER, 'waiter'),
        (RECEPTIONIST, 'receptionist')
    ]
    role = models.IntegerField(choices=role_type, default=0)
    MALE = 0
    FEMALE = 1
    sex_type = [
        (MALE, 'male'),
        (FEMALE, 'female')
    ]
    sex = models.IntegerField(choices=sex_type, default=0)
    avatar = models.FileField(upload_to='./static/backendmedia/avatar/%Y/%m/%d/', null=True)
    phone = models.CharField(max_length=11, default='')
    name = models.CharField(max_length=50, default='')


class Category(models.Model):
    category_name = models.CharField(max_length=50, unique=True)


class Dish(models.Model):
    name = models.CharField(max_length=50)
    expected_time = models.IntegerField()
    price = models.FloatField()
    image = models.FileField(upload_to='./static/backendmedia/%Y/%m/%d/', null=True)
    category = models.ForeignKey(Category, related_name='category_dish', on_delete=models.SET_NULL, null=True)


class Table(models.Model):
    number = models.IntegerField(default=0, unique=True)
    AVAILABLE = 0
    TO_BE_CLEANED = 1
    IN_USE = 2
    state_type = [
        (AVAILABLE, 'available'),
        (TO_BE_CLEANED, 'to_be_cleaned'),
        (IN_USE, 'in_use'),
    ]
    state = models.IntegerField(choices=state_type, default=0)
    x = models.IntegerField(default=10)
    y = models.IntegerField(default=10)
    waiter = models.ForeignKey(Account, related_name='waiter_table', on_delete=models.SET_NULL, null=True)


class Order(models.Model):
    total_price = models.FloatField(default=0)
    table = models.ForeignKey(Table, related_name='table_order', on_delete=models.CASCADE, null=False)
    UNPAID = 0
    PAID = 1
    state_type = [
        (UNPAID, 'unpaid'),
        (PAID, 'paid'),
    ]
    state = models.IntegerField(choices=state_type, default=0)
    start_time = models.DateTimeField()
    due_time = models.DateTimeField(default=None, null=True)
    people_count = models.IntegerField(default=1)


class OrderDish(models.Model):
    dish = models.ForeignKey(Dish, related_name='dish_OrderDish', on_delete=models.CASCADE, null=False)
    order = models.ForeignKey(Order, related_name='order_OrderDish', on_delete=models.CASCADE, null=False)
    undone_count = models.IntegerField(default=1)
    done_count = models.IntegerField(default=0)
    served_count = models.IntegerField(default=0)


class VerificationCode(models.Model):
    verification_code = models.CharField(max_length=6)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
