from django.urls import path, include

from rest_framework import routers


from demo.views.AccountAllowAny import AccountAllowAnyView
from demo.views.CategoryView import CategoryView
from demo.views.OrderDishView import OrderDishView
from demo.views.DishView import DishView
from demo.views.OrderView import OrderView
from demo.views.TableView import TableView
from demo.views.AccountView import AccountView

router = routers.DefaultRouter()
router.register('Account', AccountView)
router.register('AccountAllowAny', AccountAllowAnyView)
router.register('Table', TableView)
router.register('Dish', DishView)
router.register('Category', CategoryView)
router.register('OrderDish', OrderDishView)
router.register('Order', OrderView)

urlpatterns = [
    path('', include(router.urls)),
]
