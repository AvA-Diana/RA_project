module.exports = {
    "env": {
        "browser": true,
        "es2021": true
    },
    "extends": [
        "airbnb"
    ],
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": 13,
        "sourceType": "module"
    },
    "plugins": [
        "react"
    ],
    "rules": {
        "react/jsx-filename-extension": ["warn", {"extensions": [".js", ".jsx"]}],
        "import/extensions": [2, "never", { "web.js": "never", "json": "never" }],
        "import/no-extraneous-dependencies": [2, { "devDependencies": true }],
        "import/no-unresolved": [2, { "ignore": ["antd-mobile"]}]
    },
    "parser": "babel-eslint"
};

