import axios from 'axios';

/**
 * 得到桌子状态
 * @param {number} number
 */
export const getAvailableTable = (number) => axios.post('Table/delete_table_by_number/', { number })
  .then((response) => response.data);

/**
 * 移动桌子
 * @param {number} id
 * @param {number} x
 * @param {number} y
 * @returns
 */
export const moveTable = (id, x, y) => axios.post(`Table/${id}/move_table/`, {
  x,
  y,
}).then((response) => response.data);

/**
 * 得到桌子位置
 * @returns
 */
export const getTables = () => axios.get('Table/get_tables/')
  .then((response) => response.data);

/**
 * 添加桌子
 * @param {number} number
 */
export const addTable = (number) => axios.post('Table/add_table/', { number })
  .then((response) => response.data);

/**
 * 删除桌子
 * @param {number} number
 */
export const deleteTable = (number) => axios.post('Table/delete_table_by_number/', { number })
  .then((response) => response.data);

/**
 * 占用桌子并创建订单
 * @param {number} table
 * @param {string} startTime
 * @param {number} peopleCount
 */
export const toOccupyTable = (table, startTime, peopleCount) => axios.post('Table/occupy_table/', {
  table,
  start_time: startTime,
  people_count: peopleCount,
})
  .then((response) => response.data);

/**
 * 释放桌子并关闭订单
 * @param {number} table
 * @param {string} dueTime
 */
export const toReleaseTable = (table, dueTime) => axios.post('Table/release_table/', {
  table,
  due_time: dueTime,
})
  .then((response) => response.data);

/**
 * 清洁桌子
 * @param {number} table
 */
export const toCleanTable = (table) => axios.post('Table/clean_table/', {
  table,
})
  .then((response) => response.data);
