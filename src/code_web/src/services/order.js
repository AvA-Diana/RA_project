import axios from 'axios';

/**
 * 为订单加菜
 * @param {number} table
 * @param {Array} shoppingCar
 * @param {number} sum
 */
export const toAddDishes = (table, shoppingCar, sum) => {
  const dishesId = [];
  shoppingCar[0].forEach((dish) => {
    dishesId.push(dish.id);
  });
  return (
    axios.post('OrderDish/take_order/', {
      table,
      dishes_id: dishesId,
      dish_counts: shoppingCar[1],
      sum,
    })
      .then((response) => response.data)
  );
};

/**
 * 查询所有已完成但未上菜完成订单
 */
export const getFinishedDishes = () => axios.get('OrderDish/get_finished_dishes/')
  .then((response) => response.data);

/**
 * 查询订单
 * @param {number} table
 */
export const toInquiryDishes = (table) => (
  axios.post('OrderDish/inquiry_order/', {
    table,
  })
    .then((response) => response.data)
);

/**
 * 完成订单
 * @param {number} id
 * @param {number} count
 */
export const finishDishes = (id, count) => (
  axios.post(`OrderDish/${id}/order_dishes_done/`, {
    count,
  })
    .then((response) => response.data)
);

/**
 * 服务员上菜
 * @param {number} id
 * @param {number} count
 */
export const serveDishes = (id, count) => (
  axios.post(`OrderDish/${id}/order_dishes_served/`, {
    count,
  })
    .then((response) => response.data)
);

/**
 * 建立WebSocket连接
 * @param {string} url
 */
export const webSocketConnect = (url) => new WebSocket(url);

/**
 * 更改菜品照片
 * @param file
 * @param {number} id
 */
export const modifyDishImage = (file, id) => {
  const formData = new FormData();
  formData.append('image', file);
  return axios.post(`Dish/${id}/modify_dish_image/`, formData)
    .then((res) => res.data)
    .catch(() => false);
};
