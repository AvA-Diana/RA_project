import axios from 'axios';

/**
 * 用户注册
 * @param {object} values
 */
export const register = (values) => axios.post('AccountAllowAny/register/', {
  password: values.password,
  role_type: values.role_type,
  sex_type: values.sex_type,
  name: values.name,
  email: values.email,
  phone: values.phone,
})
  .then((response) => response.data);

/**
 * 用户登录
 * @param {object} values
 */
export const login = (values) => axios.post('AccountAllowAny/login/', {
  username: values.username,
  password: values.password,
})
  .then((response) => response.data);
/**
 * 获取当前登录账号的信息
 */
export const getLoginAccountInfo = () => axios.get('Account/get_login_account_info/')
  .then((response) => response.data);

/**
 * 用户退出
 */
export const logout = () => axios.post('Account/logout/')
  .then((response) => response.data);

/**
 * 发送验证码
 * @param {string} username
 */
export const sendVerificationCode = (username) => axios.post('AccountAllowAny/send_verification_code/', {
  username,
})
  .then((response) => response.data);

/**
 * 重置密码
 * @param {string} username
 * @param {string} verificationCode
 * @param {string} password
 */
export const resetPassword = (username, verificationCode, password) => axios.post('AccountAllowAny/retrieve_password/', {
  username,
  verification_code: verificationCode,
  password,
})
  .then((response) => response.data);

/**
 * 设置cookie
 * @param {string} key
 * @param {boolean} value
 * @param {number} day
 */
export const setCookie = (key, value, day) => {
  const expires = day * 86400 * 1000; // 时间转化成 ms
  const date = new Date(+new Date() + expires); // 当前时间加上要存储的时间
  document.cookie = `${key}=${value};expires=${date.toUTCString()}`;
};

/**
 * 更改用户信息
 * @param {number} id
 * @param {object} values
 */
export const resetAccountInfo = (id, values) => axios.post(`Account/${id}/reset_account_info/`, {
  name: values.name,
  sex: values.sex === '男' ? 'male' : 'female',
  phone: values.phone,
  email: values.email,
})
  .then((response) => response.data);

/**
 * 更改头像
 */
export const modifyAvatar = (file) => {
  const formData = new FormData();
  formData.append('avatar', file);
  return axios.post('Account/modify_avatar/', formData)
    .then((res) => res.data)
    .catch(() => false);
};
/**
 * 删除
 * @param {number} id
 */
export const deleteAccount = (id) => axios.post(`Account/${id}/delete_account/`)
  .then((response) => response.data);
