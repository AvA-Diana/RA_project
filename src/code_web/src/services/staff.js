import axios from 'axios';

/**
 * 获取用户列表
 *  * @param {string} type
 */
export const getStaffList = (type) => axios.post('Account/get_staff/', {
  role_type: type,
})
  .then((response) => response.data);

/**
 * 获取单个用户
 *  * @param {number} id
 */
export const getStaff = (id) => axios.get(`Account/${id}/get_account_info/`)
  .then((response) => response.data);

/**
 * 用户退出
 */
export const logout = () => axios.post('Account/logout/')
  .then((response) => response.data);
