import axios from 'axios';

/**
 * 得到 Date 对象的格式化字符串
 */
export const getFormatDate = () => {
  const date = new Date();
  return `${date.getFullYear()}-${date.getMonth()}-${date.getDate()} ${date.getHours()}:${date.getMinutes()}`;
};

export const geta = () => axios.get('Category/get_categories/')
  .then((response) => response.data);
