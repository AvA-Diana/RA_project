import axios from 'axios';

/**
 * 得到菜品种类
 * @returns
 */
export const getCategories = () => axios.get('Category/get_categories/')
  .then((response) => response.data);

/**
 * 修改菜品种类信息
 * @param {list} categories
 * @param {list} deleteCategories
 * @returns
 */
export const modifyCategories = (categories, deleteCategories) => axios.post('Category/modify_categories/', {
  categories,
  delete_categories: deleteCategories,
})
  .then((response) => response.data);

/**
 * 依据菜品种类得到菜品
 * @returns
 */
export const getDishes = () => axios.get('Dish/get_dishes/')
  .then((response) => {
    if (response.data !== 'failed') {
      return response.data;
    }
    return false;
  });

/**
 * 添加菜品信息
 * @param {object} values
 * @returns
 */
export const addDish = (values) => axios.post('Dish/add_dish/', {
  name: values.name,
  price: values.price,
  category: values.category,
})
  .then((response) => response.data);

/**
 * 修改菜品信息
 * @param {object} values
 * @param {number} id
 * @returns
 */
export const modifyDish = (values, id) => axios.post(`Dish/${id}/modify_dish/`, {
  name: values.name,
  price: values.price,
  category: values.category,
})
  .then((response) => response.data);

/**
 * 删除菜品
 * @param {number} id
 * @returns
 */
export const deleteDish = (id) => axios.post(`Dish/${id}/delete_dish/`)
  .then((response) => response.data);
