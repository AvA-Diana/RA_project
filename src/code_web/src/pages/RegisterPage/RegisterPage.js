import React from 'react';
import RegisterBox from '../../component/RegisterBox/RegisterBox';
import LayoutComponent from '../../component/Layout-component/Layout-component';

const RegisterPage = () => (
  <LayoutComponent Box={RegisterBox} title="注册账号" />
);

export default RegisterPage;
