import React from 'react';
import LoginBox from '../../component/LoginBox/LoginBox';
import LayoutComponent from '../../component/Layout-component/Layout-component';

const LoginPage = () => (
  <LayoutComponent Box={LoginBox} title="餐厅管理系统" />
);

export default LoginPage;
