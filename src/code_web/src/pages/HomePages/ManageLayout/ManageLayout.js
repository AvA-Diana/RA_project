import React, { useEffect, useState } from 'react';
import {
  Button, Input, message, Space,
} from 'antd';
import { PlusOutlined, DeleteOutlined } from '@ant-design/icons';
import Modal from 'antd/es/modal/Modal';
import Table from '../../../component/Table/Table';
import './ManageLayout.css';
import { getTables, addTable, deleteTable } from '../../../services/table';

const ManageLayout = () => {
  const [tables, setTables] = useState([]);
  const [addTableId, setAddTableId] = useState(0);
  const [addModalVisible, setAddModalVisible] = useState(false);
  const [deleteTableId, setDeleteTableId] = useState(0);
  const [deleteModalVisible, setDeleteModalVisible] = useState(false);
  const getTablesXY = async () => {
    const res = await getTables();
    if (res !== 'failed') {
      setTables(res);
    } else {
      message.error('获取桌子信息失败，请重新再试');
    }
  };
  const handleAddTable = async () => {
    const res = await addTable(addTableId);
    if (res !== 'failed') {
      message.success('添加桌子成功');
      await getTablesXY();
    } else {
      message.error('添加失败，请重新再试');
    }
  };
  useEffect(() => {
    getTablesXY();
  }, []);
  const showAddModal = () => {
    setAddModalVisible(true);
  };

  const addTableIdOnChange = (e) => {
    setAddTableId(e.target.value);
  };

  const addModalHandleOk = async () => {
    if (/^\d+$/.test(addTableId)) {
      setAddModalVisible(false);
      await handleAddTable();
    } else {
      message.error('请输入桌子编号(全数字)');
    }
  };

  const addModalHandleCancel = () => {
    setAddModalVisible(false);
  };

  const showDeleteModal = () => {
    setDeleteModalVisible(true);
  };

  const deleteTableIdOnChange = (e) => {
    setDeleteTableId(e.target.value);
  };
  const handleDeleteTable = async () => {
    const res = await deleteTable(deleteTableId);
    if (res !== 'failed') {
      message.success('删除桌子成功');
      await getTablesXY();
    } else {
      message.error('删除失败，请重新再试');
    }
  };
  const deleteModalHandleOk = async () => {
    if (/^\d+$/.test(deleteTableId.toString())) {
      setDeleteModalVisible(false);
      await handleDeleteTable();
    } else {
      message.error('请输入桌子编号(全数字)');
    }
  };

  const deleteModalHandleCancel = () => {
    setDeleteModalVisible(false);
  };

  return (
    <>
      <div
        className="site-layout-background"
        style={{
          marginLeft: 60, marginRight: 60, minHeight: 500, position: 'relative',
        }}
      >
        {tables.map(
          (table) => (
            <Table
              key={table.id}
              table={table}
            />
          ),
        )}
      </div>
      <br />
      <Space
        style={{
          marginLeft: 60,
        }}
      >
        <Button
          type="primary"
          icon={<PlusOutlined />}
          onClick={showAddModal}
        >
          添加桌子
        </Button>
        <Button
          type="danger"
          icon={<DeleteOutlined />}
          onClick={showDeleteModal}
        >
          删除桌子
        </Button>
      </Space>
      <Modal
        title="添加桌子"
        destroyOnClose
        visible={addModalVisible}
        okText="确定"
        cancelText="取消"
        onOk={addModalHandleOk}
        onCancel={addModalHandleCancel}
      >
        <Input onChange={addTableIdOnChange} placeholder="输入桌子编号(数字即可)" />
      </Modal>
      <Modal
        title="删除桌子"
        destroyOnClose
        visible={deleteModalVisible}
        okText="确定"
        cancelText="取消"
        onOk={deleteModalHandleOk}
        onCancel={deleteModalHandleCancel}
      >
        <Input onChange={deleteTableIdOnChange} placeholder="输入桌子编号(数字即可)" />
      </Modal>
    </>
  );
};
export default ManageLayout;
