import React, { useEffect, useState } from 'react';
import { List, message } from 'antd';
import Order from '../../../component/Order/Order';
import FinishedOrderInfo from '../../../component/Order/FinishedOrderInfo';
import { webSocketConnect } from '../../../services/order';

const FinishedDishes = () => {
  const [orders, setOrders] = useState([]);

  let ws;
  let onDishesServe;
  useEffect(async () => {
    // const url = 'ws://localhost:3200/waiter/';
    const url = 'wss://www.bugmakers.top/wss/waiter/';
    ws = webSocketConnect(url);
    ws.onopen = () => {
      const getOrderMessage = { option: 'get_finished_order' };
      ws.send(JSON.stringify(getOrderMessage));
    };
    ws.onmessage = (ev) => {
      const newOrders = JSON.parse(ev.data);
      setOrders(newOrders);
    };
    ws.onerror = () => {
      message.error('获取订单失败，请刷新页面或稍后再试');
    };
    onDishesServe = () => {
      const getOrderMessage = { option: 'get_finished_order' };
      ws.send(JSON.stringify(getOrderMessage));
    };
  }, []);
  useEffect(() => () => { ws.close(); }, []);

  return (
    <div className="orders-list">
      <header className="orders-list-header">
        已完成订单
      </header>
      <List
        style={{ width: '99%' }}
        grid={{
          xs: 1,
          sm: 2,
          md: 4,
          lg: 4,
          xl: 6,
          xxl: 3,
        }}
        dataSource={orders}
        renderItem={(order) => (
          <Order
            OrderInfo={FinishedOrderInfo}
            order={order}
            description={`${order.table}号桌 ${order.count}份`}
            onFinish={onDishesServe}
          />
        )}
      />
    </div>
  );
};

export default FinishedDishes;
