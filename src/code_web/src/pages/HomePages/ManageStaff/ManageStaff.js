import React from 'react';
import { Redirect, Switch } from 'react-router-dom';
import OverView from '../../../component/ManageStaff/OverView';
import PersonInfo from '../../../component/ManageStaff/PersonInfo';
import PrivateRoute from '../../../component/PrivateRoute/PrivateRoute';

const ManageStaff = () => (
  <Switch>
    <PrivateRoute path="/home/manageStaff/:id" component={PersonInfo} />
    <PrivateRoute exact path="/home/manageStaff" component={OverView} />
    <Redirect path="*" to={{ pathname: '' }} />
  </Switch>
);

export default ManageStaff;
