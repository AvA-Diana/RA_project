import React, { useEffect, useState } from 'react';
import {
  Button, Col, message, Row, Tabs,
} from 'antd';
import { Sticky, StickyContainer } from 'react-sticky';
import './ManageMenu.css';
import { EditOutlined, PlusOutlined, ProfileOutlined } from '@ant-design/icons';
import Dish from '../../../component/Dish/Dish';
import ModifyCategoryComponent from '../../../component/ModifyCategoryComponent/ModifyCategoryComponent';
import {
  getCategories, getDishes, addDish, modifyDish, deleteDish, modifyCategories,
} from '../../../services/menu';
import DishInfo from '../../../component/Dish/DishInfo';

const ManageMenu = () => {
  const [categories, setCategories] = useState([]);
  const [dishes, setDishes] = useState([]);
  const [modifyCategoryVisible, setModifyCategoryVisible] = useState(false);
  // eslint-disable-next-line no-unused-vars
  const [addDishVisible, setAddDishVisible] = useState(false);

  const setAllDishes = async (localCategories) => {
    const allDishes = await getDishes();
    if (allDishes === false) {
      message.error('获取菜单信息失败，请重新再试');
    } else {
      const categoryDic = {};
      const nowDishes = [];
      for (let i = 0; i <= localCategories.length; i += 1) {
        nowDishes[i] = [];
      }
      for (let i = 0; i < localCategories.length; i += 1) {
        categoryDic[localCategories[i].id] = i;
      }
      allDishes.forEach((dish) => {
        if (categoryDic[dish.category] !== undefined) {
          nowDishes[categoryDic[dish.category]].push(dish);
        } else {
          nowDishes[nowDishes.length - 1].push(dish);
        }
      });
      setDishes(nowDishes);
    }
  };

  const setAllCategories = async () => {
    const categoriesResponse = await getCategories();
    if (categoriesResponse === 'failed') {
      message.error('获取菜品信息失败，请重新再试');
    } else {
      setCategories(categoriesResponse);
      await setAllDishes(categoriesResponse);
    }
  };

  useEffect(setAllCategories, []);

  const renderTabBar = (props, DefaultTabBar) => (
    <Sticky bottomOffset={80}>
      {({ style }) => (
        // eslint-disable-next-line react/jsx-props-no-spreading
        <DefaultTabBar {...props} className="site-custom-tab-bar" style={{ ...style }} />
      )}
    </Sticky>
  );

  const modifyMenuButtons = (
    <>
      <Button
        type="primary"
        icon={<PlusOutlined />}
        style={{ marginRight: '10px' }}
        onClick={() => { setAddDishVisible(true); }}
      >
        添加新的菜品
      </Button>
      <Button
        type="primary"
        icon={<ProfileOutlined />}
        style={{ marginRight: '10px' }}
        onClick={() => { setModifyCategoryVisible(true); }}
      >
        修改菜品种类
      </Button>
    </>
  );

  const onModifyCategories = async (modifiedCategories, deletedCategories) => {
    const res = await modifyCategories(modifiedCategories, deletedCategories);
    if (res === 'succeed') {
      await setAllCategories();
      setModifyCategoryVisible(false);
      message.success('修改成功！');
    } else {
      message.error('修改失败，请稍后再试');
    }
  };

  const onAddDish = async (values) => {
    const res = await addDish(values);
    if (res === 'succeed') {
      message.success('添加成功！');
      setAddDishVisible(false);
      await setAllDishes(categories);
    } else {
      message.error('添加失败，请稍后再试');
    }
  };

  const onModifyDish = async (values, id) => {
    const res = await modifyDish(values, id);
    if (res === 'succeed') {
      message.success('修改成功！');
      await setAllDishes(categories);
    } else {
      message.error('修改失败，请稍后再试');
    }
  };

  const onDeleteDish = async (id) => {
    const res = await deleteDish(id);
    if (res === 'succeed') {
      message.success('删除成功！');
      await setAllDishes(categories);
    } else {
      message.error('删除失败，请稍后再试');
    }
  };

  const { TabPane } = Tabs;

  return (
    <>
      <StickyContainer>
        <Tabs
          className="menu"
          renderTabBar={renderTabBar}
          tabBarExtraContent={modifyMenuButtons}
          style={{ marginLeft: 15 }}
        >
          <TabPane className="TabPane" tab="所有菜品" key={0}>
            <Row gutter={[16, 25]}>
              {dishes.map((categorizeDishes) => categorizeDishes.map((dish) => (
                <Col span={6}>
                  <Dish
                    dish={dish}
                    categories={categories}
                    buttonOutlined={<EditOutlined />}
                    buttonText="修改菜品信息"
                    onFinish={onModifyDish}
                    onDelete={async () => { await onDeleteDish(dish.id); }}
                  />
                </Col>
              )))}
            </Row>
          </TabPane>
          {categories.map((category, i) => {
            const item = typeof (dishes[i]) === 'object' ? dishes[i].map((dish) => (
              <Col span={6}>
                <Dish
                  dish={dish}
                  categories={categories}
                  buttonOutlined={<EditOutlined />}
                  buttonText="修改菜品信息"
                  onFinish={onModifyDish}
                  onDelete={async () => { await onDeleteDish(dish.id); }}
                />
              </Col>
            )) : '';
            return (
              <TabPane tab={category.category_name} key={category.id}>
                <Row gutter={[16, 25]}>
                  {item}
                </Row>
              </TabPane>
            );
          })}
          <TabPane tab="未分类" key={10086}>
            <Row gutter={[16, 25]}>
              {typeof (dishes[dishes.length - 1]) === 'object' ? dishes[dishes.length - 1].map((dish) => (
                <Col span={6}>
                  <Dish
                    dish={dish}
                    categories={categories}
                    buttonOutlined={<EditOutlined />}
                    buttonText="修改菜品信息"
                    onFinish={onModifyDish}
                    onDelete={async () => { await onDeleteDish(dish.id); }}
                  />
                </Col>
              )) : ''}
            </Row>
          </TabPane>
        </Tabs>
      </StickyContainer>
      <ModifyCategoryComponent
        categories={categories}
        visible={modifyCategoryVisible}
        onModify={onModifyCategories}
        onCancel={() => { setModifyCategoryVisible(false); }}
      />
      <DishInfo
        visible={addDishVisible}
        categories={categories}
        type="add"
        onFinish={onAddDish}
        onCancel={() => { setAddDishVisible(false); }}
      />
    </>
  );
};
export default ManageMenu;
