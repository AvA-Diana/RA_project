import React, { useEffect, useState } from 'react';
import {
  Layout, Menu, Avatar, Dropdown, message,
} from 'antd';
import {
  HomeOutlined, FileDoneOutlined, BarChartOutlined,
  TableOutlined, ProfileOutlined, TeamOutlined,
  InteractionOutlined, LogoutOutlined, KeyOutlined,
} from '@ant-design/icons';
import { Content, Header } from 'antd/es/layout/layout';
import PropTypes from 'prop-types';
import {
  Redirect, Switch, withRouter,
} from 'react-router-dom';
import Home from '../Home/Home';
import ManageMenu from '../ManageMenu/ManageMenu';
import ManageLayout from '../ManageLayout/ManageLayout';
import ManageStaff from '../ManageStaff/ManageStaff';
import RestaurantData from '../RestaurantData/RestaurantData';
import RestaurantInformation from '../RestaurantInformation/RestaurantInformation';
import FinishedOrders from '../FinishedOrders/FinishedOrders';
import OrderDish from '../OrderDish/OrderDish';
import './PrimaryPage.css';
import FooterComponent from '../../../component/Footer-component/Footer-component';
import {
  getLoginAccountInfo, logout, setCookie,
} from '../../../services/account';
import PrivateRoute from '../../../component/PrivateRoute/PrivateRoute';
import ModifyPasswordComponent from '../../../component/ModifyPasswordComponent/ModifyPasswordComponent';
import imgURL from '../../../assets/images/RestaurantName.png';
import ModifyInfoComponent from '../../../component/ModifyInfoComponent/ModifyInfoComponent';

const {
  Sider,
} = Layout;

const PrimaryPage = (props) => {
  const { history } = props;
  const [isModifyPasswordModalVisible, setIsModifyPasswordModalVisible] = useState(false);
  const [isModifyInfoModalVisible, setIsModifyInfoModalVisible] = useState(false);
  const [user, setUser] = useState('');
  const [defaultSelect] = useState([]);
  const getLoginInfo = async () => {
    const res = await getLoginAccountInfo();
    setUser(res);
  };
  const getDefaultKey = (pathname) => {
    switch (pathname) {
      case 'finishedOrder':
        return '1';
      case 'manageMenu':
        return '2';
      case 'manageLayout':
        return '3';
      case 'manageStaff':
        return '4';
      case 'restaurantData':
        return '5';
      case 'restaurantInformation':
        return '6';
      default:
        return '0';
    }
  };
  useEffect(() => {
    getLoginInfo();
    const pathname = history.location.pathname.split('/');
    for (let pathnameKey = 0; pathnameKey < pathname.length; pathnameKey += 1) {
      if (pathname[pathnameKey] === 'home') {
        if (pathname.length === Number(pathnameKey) + 1) {
          defaultSelect[0] = '0';
        } else {
          defaultSelect[0] = getDefaultKey(pathname[Number(pathnameKey) + 1]);
        }
      }
    }
  }, [props]);
  const handleMenuClick = async (e) => {
    if (e.key === 'modifyPassword') {
      setIsModifyPasswordModalVisible(true);
    } else {
      const res = await logout();
      if (res === 'succeed') {
        message.success('退出成功');
        history.replace('/login');
        setCookie('login', false, -1);
      } else {
        message.error('退出失败，请重新再试');
      }
    }
  };
  const updateModalVisible = (IsModalVisible) => {
    setIsModifyPasswordModalVisible(IsModalVisible);
  };
  const updateModifyInfoModalModalVisible = (IsModalVisible) => {
    setIsModifyInfoModalVisible(IsModalVisible);
  };
  const menu = (
    <Menu onClick={handleMenuClick}>
      <Menu.Item key="modifyPassword" icon={<KeyOutlined />}>
        修改密码
      </Menu.Item>
      <Menu.Item key="logout" danger icon={<LogoutOutlined />}>
        退出登陆
      </Menu.Item>
    </Menu>
  );
  return (
    <>
      <Layout style={{ minHeight: '100vh' }}>
        <Sider
          style={{
            height: '100vh',
            position: 'fixed',
          }}
        >
          <div className="primary-page-avatar">
            <Dropdown overlay={menu} arrow>
              <Avatar
                className="avatar"
                size={49}
                src={user.avatar}
                alt={user.name}
                onClick={() => setIsModifyInfoModalVisible(true)}
              />
            </Dropdown>
            <span className="welcome-text">
              欢迎你
              <p style={{ marginLeft: '60%', marginTop: '-10%' }}>{user.name}</p>
            </span>
          </div>

          <Menu theme="dark" defaultSelectedKeys={defaultSelect} mode="inline">
            <Menu.Item
              key="0"
              icon={<HomeOutlined />}
              onClick={() => history.push('/home')}
            >
              餐厅
            </Menu.Item>
            <Menu.Item
              key="1"
              icon={<FileDoneOutlined />}
              onClick={() => history.push('/home/finishedOrder')}
              disabled={user.role !== '服务员' && user.role !== '经理'}
            >
              已完成订单
            </Menu.Item>
            <Menu.Item
              key="2"
              icon={<ProfileOutlined />}
              onClick={() => history.push('/home/manageMenu')}
              disabled={user.role !== '经理'}
            >
              修改菜单信息
            </Menu.Item>
            <Menu.Item
              key="3"
              icon={<TableOutlined />}
              onClick={() => history.push('/home/manageLayout')}
              disabled={user.role !== '经理'}
            >
              更改餐厅布局
            </Menu.Item>
            <Menu.Item
              key="4"
              icon={<TeamOutlined />}
              onClick={() => history.push('/home/manageStaff')}
              disabled={user.role !== '经理'}
            >
              管理餐厅员工
            </Menu.Item>
            <Menu.Item
              key="5"
              icon={<BarChartOutlined />}
              onClick={() => history.push('/home/restaurantData')}
              disabled={user.role !== '经理'}
            >
              餐厅经营状况
            </Menu.Item>
            <Menu.Item
              key="6"
              icon={<InteractionOutlined />}
              onClick={() => history.push('/home/restaurantInformation')}
              disabled={user.role !== '经理'}
            >
              更改餐厅信息
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout className="site-layout" style={{ marginLeft: 200 }}>
          <Header className="site-layout-background" style={{ padding: 0 }}>
            <div>
              <img src={imgURL} alt="logo" height="55px" width="160px" />
            </div>
          </Header>
          <Content style={{ margin: '30px 16px 10px 16px' }}>
            <Switch>
              <PrivateRoute path="/home/manageMenu" component={ManageMenu} />
              <PrivateRoute path="/home/finishedOrder" component={FinishedOrders} />
              <PrivateRoute path="/home/manageLayout" component={ManageLayout} />
              <PrivateRoute path="/home/manageStaff" component={ManageStaff} />
              <PrivateRoute path="/home/restaurantData" component={RestaurantData} />
              <PrivateRoute path="/home/restaurantInformation" component={RestaurantInformation} />
              <PrivateRoute path="/home/orderDish/:id" component={OrderDish} />
              <PrivateRoute exact path="/home" component={Home} role={user.role} />
              <Redirect path="*" to={{ pathname: '/home' }} />
            </Switch>
          </Content>
          <FooterComponent />
        </Layout>
      </Layout>
      <ModifyPasswordComponent
        isModifyPasswordModalVisible={isModifyPasswordModalVisible}
        updateParent={updateModalVisible}
      />
      <ModifyInfoComponent
        isModifyInfoModalVisible={isModifyInfoModalVisible}
        updateParent={updateModifyInfoModalModalVisible}
        succeedCallBack={getLoginInfo}
      />
    </>
  );
};

export default withRouter(PrimaryPage);
PrimaryPage.defaultProps = {
  history: '',
};

PrimaryPage.propTypes = {
  history: PropTypes.objectOf(PropTypes.any),
};
