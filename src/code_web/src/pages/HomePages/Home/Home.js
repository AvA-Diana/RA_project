import React, { useEffect, useState } from 'react';
import { message } from 'antd';
import PropTypes from 'prop-types';
import { getTables } from '../../../services/table';
import UnfinishedDishes from './UnfinishedDishes';
import Tables from './Tables';
import './Home.css';

const Home = (props) => {
  const { role } = props;
  const [tables, setTables] = useState([]);
  const getTablesXY = async () => {
    const res = await getTables();
    if (res !== 'failed') {
      setTables(res);
    } else {
      message.error('获取桌子信息失败，请重新再试');
    }
  };
  useEffect(async () => {
    if (role !== '厨师') { await getTablesXY(); }
  }, []);

  return (
    role === '厨师'
      ? <UnfinishedDishes />
      : <Tables tables={tables} />
  );
};

export default Home;

Home.defaultProps = {
  role: '',
};

Home.propTypes = {
  role: PropTypes.string,
};
