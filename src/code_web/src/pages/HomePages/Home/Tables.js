import React from 'react';
import PropTypes from 'prop-types';
import Table from '../../../component/Table/Table';

const Tables = (props) => {
  const { tables } = props;

  return (
    <div
      className="site-layout-background"
      style={{
        marginLeft: 60, marginRight: 60, minHeight: 500, position: 'relative',
      }}
    >
      {tables.map(
        (table) => (
          <Table
            key={table.id}
            table={table}
            draggable={false}
          />
        ),
      )}
    </div>
  );
};

export default Tables;

Tables.defaultProps = {
  tables: [],
};

Tables.propTypes = {
  tables: PropTypes.arrayOf(PropTypes.object),
};
