import React, { useEffect, useState } from 'react';
import { List, message } from 'antd';
import Order from '../../../component/Order/Order';
import UnfinishedOrderInfo from '../../../component/Order/UnfinishedOrderInfo';
import { webSocketConnect } from '../../../services/order';
import './UnfinishedDishes.css';

const UnfinishedDishes = () => {
  const [orders, setOrders] = useState([]);

  let ws;
  let onDishesFinish;
  useEffect(async () => {
    const url = 'wss://www.bugmakers.top/wss/cook/';
    ws = webSocketConnect(url);
    ws.onopen = () => {
      const getOrderMessage = { option: 'get_order' };
      ws.send(JSON.stringify(getOrderMessage));
    };
    ws.onmessage = (ev) => {
      const newOrders = JSON.parse(ev.data);
      setOrders(newOrders);
    };
    ws.onerror = () => {
      message.error('获取订单失败，请刷新页面或稍后再试');
    };
    onDishesFinish = () => {
      const getOrderMessage = { option: 'get_order' };
      ws.send(JSON.stringify(getOrderMessage));
    };
  }, []);
  useEffect(() => () => { ws.close(); }, []);

  return (
    <div className="orders-list">
      <header className="orders-list-header">
        未完成订单
      </header>
      <List
        style={{ width: '99%' }}
        grid={{
          xs: 1,
          sm: 2,
          md: 4,
          lg: 4,
          xl: 6,
          xxl: 3,
        }}
        dataSource={orders}
        renderItem={(order) => (
          <Order
            OrderInfo={UnfinishedOrderInfo}
            order={order}
            description={`${order.count}份`}
            onFinish={onDishesFinish}
          />
        )}
      />
    </div>
  );
};

export default UnfinishedDishes;
