import React, { useEffect, useState } from 'react';
import {
  Badge,
  Button, Col, message, Row, Tabs,
} from 'antd';
import { CheckOutlined, PlusOutlined, ShoppingCartOutlined } from '@ant-design/icons';
import { Sticky, StickyContainer } from 'react-sticky';
import './OrderDish.css';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import ShoppingCar from '../../../component/ShoppingCar/ShoppingCar';
import OrderedCar from '../../../component/OrderedCar/OrderedCar';
import Dish from '../../../component/Dish/Dish';
import { getCategories, getDishes } from '../../../services/menu';

const OrderDish = (props) => {
  const { match, history } = props;
  // shoppingCar 作为一个二维数组，第一行存储 dish 对象，第二行存储对应的 dish 的数量
  const [shoppingCar, setShoppingCar] = useState([[], []]);
  const [shoppingCarVisible, setShoppingCarVisible] = useState(false);
  const [orderedCarVisible, setOrderedCarVisible] = useState(false);
  const [categories, setCategories] = useState([]);
  const [dishes, setDishes] = useState([]);
  const [dishesCount, setDishesCount] = useState(0);

  const loadCategories = async () => {
    const categoriesResponse = await getCategories();
    if (categoriesResponse === 'failed') {
      message.error('获取菜品信息失败，请重新再试');
    } else {
      setCategories(categoriesResponse);
    }

    if (categoriesResponse !== 'failed') {
      const allDishes = await getDishes(categoriesResponse);
      if (allDishes === false) {
        message.error('获取菜单信息失败，请重新再试');
      } else {
        const categoryDic = {};
        const nowDishes = [];
        for (let i = 0; i <= categoriesResponse.length; i += 1) {
          nowDishes[i] = [];
        }
        for (let i = 0; i < categoriesResponse.length; i += 1) {
          categoryDic[categoriesResponse[i].id] = i;
        }
        allDishes.forEach((dish) => {
          if (categoryDic[dish.category] !== undefined) {
            nowDishes[categoryDic[dish.category]].push(dish);
          } else {
            nowDishes[nowDishes.length - 1].push(dish);
          }
        });
        setDishes(nowDishes);
      }
    }
  };

  useEffect(() => {
    loadCategories();
  }, []);

  const handleClearShoppingCar = () => {
    setShoppingCar([[], []]);
    setDishesCount(0);
  };

  const handleDishReduce = (dish) => {
    const tempShoppingCar = shoppingCar.map((row) => [...row]);
    if (tempShoppingCar[0].includes(dish)) {
      if (tempShoppingCar[1][tempShoppingCar[0].indexOf(dish)] === 1) {
        const index = tempShoppingCar[0].indexOf(dish);
        tempShoppingCar[0].splice(index, 1);
        tempShoppingCar[1].splice(index, 1);
      } else {
        tempShoppingCar[1][tempShoppingCar[0].indexOf(dish)] -= 1;
      }
    }
    setShoppingCar(tempShoppingCar);
    setDishesCount(dishesCount - 1);
  };

  const handleDishAdd = (dish) => {
    const tempShoppingCar = shoppingCar.map((row) => [...row]);
    if (tempShoppingCar[0].includes(dish)) {
      tempShoppingCar[1][tempShoppingCar[0].indexOf(dish)] += 1;
    }
    setShoppingCar(tempShoppingCar);
    setDishesCount(dishesCount + 1);
  };

  const shoppingCarButtons = (
    <>
      <Button
        type="primary"
        className="button"
        icon={<CheckOutlined />}
        onClick={() => {
          setOrderedCarVisible(true);
        }}
      >
        已下单
      </Button>
      <Badge count={dishesCount} offset={[-15, 5]}>
        <Button
          type="primary"
          className="button"
          icon={<ShoppingCartOutlined />}
          onClick={() => {
            setShoppingCarVisible(true);
          }}
        >
          购物车
        </Button>
      </Badge>
      <Button
        type="primary"
        className="button"
        onClick={history.goBack}
      >
        返回
      </Button>
    </>
  );

  const onClickDish = (dish) => {
    if (shoppingCar[0].includes(dish)) {
      shoppingCar[1][shoppingCar[0].indexOf(dish)] += 1;
    } else {
      shoppingCar[0].push(dish);
      shoppingCar[1].push(1);
    }
    setDishesCount(dishesCount + 1);
    message.success('已将该菜品加入购物车!', 0.5);
  };

  const renderTabBar = (localProps, DefaultTabBar) => (
    <Sticky bottomOffset={80}>
      {({ style }) => (
        // eslint-disable-next-line react/jsx-props-no-spreading
        <DefaultTabBar {...localProps} className="site-custom-tab-bar" style={{ ...style }} />
      )}
    </Sticky>
  );

  const { TabPane } = Tabs;
  return (

    <>
      <StickyContainer>
        <Tabs
          className="menu"
          renderTabBar={renderTabBar}
          tabBarExtraContent={shoppingCarButtons}
          style={{ marginLeft: 15 }}
        >
          {categories.map((category, i) => {
            const item = typeof (dishes[i]) === 'object' ? dishes[i].map((dish) => (
              <Col span={6}>
                <Dish dish={dish} buttonOutlined={<PlusOutlined />} onClickDish={() => onClickDish(dish)} buttonText="点击下单" />
              </Col>
            )) : '';
            return (
              <TabPane className="TabPane" tab={category.category_name} key={category.id}>
                <Row gutter={[16, 25]}>
                  {item}
                </Row>
              </TabPane>
            );
          })}
          <TabPane tab="未分类" key={0}>
            <Row gutter={[16, 25]}>
              {typeof (dishes[dishes.length - 1]) === 'object' ? dishes[dishes.length - 1].map((dish) => (
                <Col span={6}>
                  <Dish dish={dish} buttonOutlined={<PlusOutlined />} buttonText="点击下单" onClickDish={() => onClickDish(dish)} />
                </Col>
              )) : ''}
            </Row>
          </TabPane>
        </Tabs>
      </StickyContainer>
      <ShoppingCar
        tableId={match.params.id}
        propsShoppingCarVisible={shoppingCarVisible}
        shoppingCar={shoppingCar}
        onClose={() => setShoppingCarVisible(false)}
        clearShoppingCar={handleClearShoppingCar}
        dishReduce={handleDishReduce}
        dishAdd={handleDishAdd}
      />
      <OrderedCar
        tableId={match.params.id}
        propsOrderedCarVisible={orderedCarVisible}
        onClose={() => setOrderedCarVisible(false)}
      />
    </>
  );
};
export default withRouter(OrderDish);

OrderDish.defaultProps = {
  match: '',
  history: '',
};

OrderDish.propTypes = {
  match: PropTypes.objectOf(PropTypes.any),
  history: PropTypes.objectOf(PropTypes.any),
};
