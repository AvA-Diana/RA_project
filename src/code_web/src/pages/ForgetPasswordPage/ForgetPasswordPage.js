import React from 'react';
import ForgetPasswordBox from '../../component/ForgetPasswordBox/ForgetPasswordBox';
import LayoutComponent from '../../component/Layout-component/Layout-component';

const ForgetPasswordPage = () => (
  <LayoutComponent Box={ForgetPasswordBox} title="餐厅管理系统" />
);

export default ForgetPasswordPage;
