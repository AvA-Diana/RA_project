/**
 * 桌子状态枚举
 */
export default {
  AVAILABLE: 0,
  TO_BE_CLEANED: 1,
  IN_USE: 2,
};
