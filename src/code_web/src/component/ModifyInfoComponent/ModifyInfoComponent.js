import React, { useEffect, useState } from 'react';
import {
  Modal, Button, message, Descriptions, Input, Select, Col, Row,
} from 'antd';
import PropTypes from 'prop-types';
import { getLoginAccountInfo, modifyAvatar, resetAccountInfo } from '../../services/account';
import UploadImage from '../UploadImage/UploadImage';

const ModifyInfoComponent = (props) => {
  const { isModifyInfoModalVisible, updateParent } = props;
  const [isModalVisible, setIsModalVisible] = useState(isModifyInfoModalVisible);
  const [loading, setLoading] = React.useState(false);
  const [data, setData] = useState('');
  const [edit, setEdit] = useState(false);

  const getLoginInfo = async () => {
    const res = await getLoginAccountInfo();
    if (res !== 'failed') {
      setData(res);
    } else {
      message.error('获取用户信息失败，请重新再试');
    }
  };

  useEffect(() => {
    setIsModalVisible(isModifyInfoModalVisible);
    if (isModifyInfoModalVisible) {
      getLoginInfo();
    }
  }, [props]);

  const handleCancel = () => {
    updateParent(false);
    setIsModalVisible(false);
  };
  const onSubmit = async () => {
    let patt = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
    if (!patt.test(data.email)) {
      message.error('请输入正确的邮箱!');
      return;
    }
    patt = /^(17[0-9]|13[0-9]|14[5|7]|15[0-9]|18[0-9])\d{8}$/;
    if (!patt.test(data.phone)) {
      message.error('请输入正确的手机号码!');
      return;
    }
    setLoading(true);
    const res = await resetAccountInfo(data.id, data);
    if (res === 'succeed') {
      setEdit(false);
      message.success('修改成功!');
      updateParent(false);
      setIsModalVisible(false);
    } else if (res === 'Duplicate email') {
      message.error('该邮箱地址已存在，请重新尝试！');
    } else {
      message.error('修改用户信息失败，请重新尝试！');
    }
    setLoading(false);
  };

  return (
    <>
      <Modal
        title="编辑资料"
        width={600}
        visible={isModalVisible}
        onCancel={handleCancel}
        afterClose={() => props.succeedCallBack()}
        footer={[
          <Button
            key="submit"
            type="primary"
            loading={loading}
            onClick={onSubmit}
          >
            提交
          </Button>,
          <Button onClick={handleCancel}>
            取消
          </Button>,
        ]}
      >
        <Row justify="center">
          <Col>
            <UploadImage
              imageSrc={data.avatar}
              upLoadService={modifyAvatar}
              TooltipTitle="点击修改头像"
              callback={getLoginInfo}
            />
          </Col>
        </Row>
        <Descriptions
          title={data.role}
          className="information"
          extra={(
            <div>
              <Button
                type="primary"
                className="edit-info-button"
                onClick={() => setEdit(true)}
              >
                编辑
              </Button>
            </div>
          )}
        >
          <Descriptions.Item label="姓名" labelStyle={{ marginTop: 4 }}>
            {edit ? (
              <Input
                id="name"
                defaultValue={data.name}
                style={{ paddingLeft: 2 }}
                onChange={(e) => {
                  const localData = data;
                  localData.name = e.target.value;
                  setData(localData);
                }}
              />
            )
              : <p style={{ marginTop: 4 }}>{data.name}</p>}
          </Descriptions.Item>
          <Descriptions.Item label="性别" labelStyle={{ marginTop: 4, marginLeft: 100 }} span={2}>
            {edit ? (
              <Select
                key={data.sex}
                defaultValue={data.sex}
                bordered={edit}
                showArrow={false}
                onChange={(e) => {
                  const localData = data;
                  localData.sex = e;
                  setData(localData);
                }}
              >
                <Select.Option value="男">男</Select.Option>
                <Select.Option value="女">女</Select.Option>
              </Select>
            )
              : <p style={{ marginTop: 4 }}>{data.sex}</p>}
          </Descriptions.Item>
          <Descriptions.Item label="邮箱" labelStyle={{ marginTop: 4 }}>
            {edit ? (
              <Input
                id="email"
                defaultValue={data.email}
                bordered={edit}
                style={{ paddingLeft: 2 }}
                onChange={(e) => {
                  const localData = data;
                  localData.email = e.target.value;
                  localData.username = e.target.value;
                  setData(localData);
                }}
              />
            )
              : <p style={{ marginTop: 4 }}>{data.email}</p>}
          </Descriptions.Item>
          <Descriptions.Item label="电话" labelStyle={{ marginTop: 4, marginLeft: 100 }}>
            {edit ? (
              <Input
                id="phone"
                defaultValue={data.phone}
                bordered={edit}
                style={{ paddingLeft: 2 }}
                onChange={(e) => {
                  const localData = data;
                  localData.phone = e.target.value;
                  setData(localData);
                }}
              />
            )
              : <p style={{ marginTop: 4 }}>{data.phone}</p>}
          </Descriptions.Item>
        </Descriptions>
      </Modal>
    </>
  );
};

export default ModifyInfoComponent;
ModifyInfoComponent.defaultProps = {
  isModifyInfoModalVisible: false,
  updateParent: null,
  succeedCallBack: () => {},
};

ModifyInfoComponent.propTypes = {
  isModifyInfoModalVisible: PropTypes.bool,
  updateParent: PropTypes.func,
  succeedCallBack: PropTypes.func,
};
