import React from 'react';
import {
  Button, Form, Input, Select, message,
} from 'antd';
import {
  LockOutlined, UserOutlined, MailOutlined, PhoneOutlined,
} from '@ant-design/icons';
import './RegisterBox.css';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { register } from '../../services/account';

const RegisterBox = (props) => {
  const onFinish = async (values) => {
    const res = await register(values);
    if (res === 'succeed') {
      message.success('注册成功');
      props.history.push('/login');
    } else if (res === 'Duplicate username') {
      message.error('用户名重复，请更换用户名后再试');
    } else {
      message.error('注册失败，请重新再试');
    }
  };

  return (
    <Form
      name="normal_register"
      className="register-form"
      initialValues={{
        remember: true,
        role_type: 'manager',
        sex_type: 'male',
      }}
      onFinish={onFinish}
    >
      <br />
      <Form.Item name="role_type">
        <Select>
          <Select.Option value="manager">经理</Select.Option>
          <Select.Option value="busboy">清洁工</Select.Option>
          <Select.Option value="waiter">侍者</Select.Option>
          <Select.Option value="receptionist">接待员</Select.Option>
          <Select.Option value="cook">厨师</Select.Option>
        </Select>
      </Form.Item>
      <Form.Item style={{ marginBottom: 0 }}>
        <Form.Item
          name="name"
          rules={[{ required: true }]}
        >
          <Input
            prefix={<UserOutlined className="site-form-item-icon" />}
            placeholder="姓名"
          />
        </Form.Item>
      </Form.Item>
      <Form.Item name="sex_type">
        <Select>
          <Select.Option value="male">男</Select.Option>
          <Select.Option value="female">女</Select.Option>
        </Select>
      </Form.Item>
      <Form.Item
        name="email"
        hasFeedback
        rules={[
          {
            required: true,
            message: '请输入你的电子邮箱',
          },
          {
            pattern: new RegExp(/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/),
            message: '请输入有效的邮箱地址',
          },
        ]}
      >
        <Input
          prefix={<MailOutlined className="site-form-item-icon" />}
          placeholder="电子邮箱"
        />
      </Form.Item>
      <Form.Item
        name="phone"
        hasFeedback
        rules={[
          {
            required: true,
            message: '请输入你的电话号码',
          },
          {
            pattern: new RegExp(/^(13[0-9]|14[5|7]|15[0-9]|18[0-9]|17[0-9])\d{8}$/),
            message: '请输入有效的电话号码',
          },
        ]}
      >
        <Input
          prefix={<PhoneOutlined className="site-form-item-icon" />}
          placeholder="电话号码"
        />
      </Form.Item>
      <Form.Item
        name="password"
        rules={[{
          required: true,
          message: '请输入密码',
        }]}
        hasFeedback
      >
        <Input.Password
          prefix={<LockOutlined className="site-form-item-icon" />}
          placeholder="密码"
        />
      </Form.Item>
      <Form.Item
        name="password_check"
        dependencies={['password']}
        hasFeedback
        rules={[
          {
            required: true,
            message: '请再次输入密码',
          },
          ({ getFieldValue }) => ({
            validator(_, value) {
              if (!value || getFieldValue('password') === value) {
                return Promise.resolve();
              }
              return Promise.reject(new Error('密码不匹配，请重新输入'));
            },
          }),
        ]}
      >
        <Input.Password
          prefix={<LockOutlined className="site-form-item-icon" />}
          type="password"
          placeholder="再次确认密码"
        />
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit" className="register-form-button">
          注册
        </Button>
        {' '}
        或者
        <Button type="link" href="./login">返回登录界面</Button>
      </Form.Item>
    </Form>
  );
};
export default withRouter(RegisterBox);
RegisterBox.defaultProps = {
  history: {},
};

RegisterBox.propTypes = {
  history: PropTypes.objectOf(PropTypes.any),
};
