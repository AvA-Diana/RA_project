import React, { useEffect, useState } from 'react';
import {
  Button, Divider, Drawer, Space, Statistic,
} from 'antd';
import PropTypes from 'prop-types';
import { toInquiryDishes } from '../../services/order';
import LittleDish from '../LittleDish/LittleDish';

const OrderedCar = (props) => {
  const {
    tableId,
    propsOrderedCarVisible,
    onClose,
  } = props;
  const [orderedCarVisible, setOrderedCarVisible] = useState(propsOrderedCarVisible);
  const [allDishes, setAllDishes] = useState([[]]);
  const [sum, setSum] = useState(0);

  useEffect(async () => {
    setOrderedCarVisible(propsOrderedCarVisible);
    if (propsOrderedCarVisible) {
      const res = await toInquiryDishes(tableId);
      setAllDishes(res);
      let newSum = 0;
      res.forEach((item) => {
        newSum += item[0].price * (item[1] + item[2]);
      });
      setSum(newSum);
    }
  }, [props]);

  const renderDishes = (i) => (
    allDishes.map((dish) => {
      if (dish[i] !== 0) {
        return (
          <LittleDish
            dish={dish[0]}
            number={dish[i]}
            haveActions={false}
          >
            {dish}
          </LittleDish>
        );
      }
      return null;
    })
  );

  const operations = (
    <>
      <Statistic
        title="总计:"
        valueStyle={{ fontWeight: 600 }}
        suffix="元"
        value={sum}
        precision={2}
      />
      <Space>
        <Button
          type="primary"
          style={{ width: 100 }}
        >
          催促厨师
        </Button>
      </Space>
    </>
  );

  return (
    <>
      <Drawer
        title={`${tableId}号桌 已下单菜品`}
        width={300}
        placement="right"
        onClose={onClose}
        visible={orderedCarVisible}
        footer={operations}
      >
        <Divider orientation="left" style={{ fontWeight: 700 }}>已上</Divider>
        {renderDishes(1)}
        <Divider orientation="left" style={{ fontWeight: 700 }}>未上</Divider>
        {renderDishes(2)}
      </Drawer>
    </>
  );
};

export default OrderedCar;

OrderedCar.propTypes = {
  tableId: PropTypes.number.isRequired,
  propsOrderedCarVisible: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};
