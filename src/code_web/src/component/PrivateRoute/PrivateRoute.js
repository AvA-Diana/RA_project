import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { message } from 'antd';
// 这个组件将根据登录的情况, 返回一个路由

// eslint-disable-next-line react/prop-types
const PrivateRoute = ({ component: Component, ...props }) => (
// 解构赋值 将 props 里面的 component 赋值给 Component
  <Route
    /* eslint-disable-next-line react/jsx-props-no-spreading */
    {...props}
    render={(p) => {
      const login = document.cookie.includes('login=true');
      if (login) { // 如果登录了, 返回正确的路由
        // eslint-disable-next-line react/jsx-props-no-spreading
        return <Component {...props} />;
      } // 没有登录就重定向至登录页面
      message.warning('请先登录');
      return (
        <Redirect to={{
          pathname: '/login',
          state: {
            from: p.location.pathname,
          },
        }}
        />
      );
    }}
  />
);

export default PrivateRoute;
