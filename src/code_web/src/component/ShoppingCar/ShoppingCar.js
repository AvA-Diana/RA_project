import React, { useEffect, useState } from 'react';
import {
  Button, Drawer, message, Space, Statistic,
} from 'antd';
import PropTypes from 'prop-types';
import LittleDish from '../LittleDish/LittleDish';
import { toAddDishes } from '../../services/order';

const ShoppingCar = (props) => {
  const {
    tableId,
    propsShoppingCarVisible,
    shoppingCar,
    onClose,
    clearShoppingCar,
    dishReduce,
    dishAdd,
  } = props;
  const [shoppingCarVisible, setShoppingCarVisible] = useState(propsShoppingCarVisible);
  const [sum, setSum] = useState(0);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setShoppingCarVisible(propsShoppingCarVisible);
    let newSum = 0;
    shoppingCar[0].forEach((item, index) => {
      newSum += item.price * shoppingCar[1][index];
    });
    setSum(newSum);
  }, [props]);

  const addDishes = async () => {
    setLoading(true);
    const res = await toAddDishes(tableId, shoppingCar, sum);
    if (res === 'succeed') {
      message.success('下单成功');
      clearShoppingCar();
    } else {
      message.error('下单失败，请重试');
    }
    setLoading(false);
  };

  const operations = (
    <>
      <Statistic
        title="总计:"
        valueStyle={{ fontWeight: 600 }}
        suffix="元"
        value={sum}
        precision={2}
      />
      <Space>
        <Button
          type="primary"
          style={{ width: 100 }}
          onClick={addDishes}
          loading={loading}
        >
          下单
        </Button>
        <Button
          type="danger"
          style={{ width: 100 }}
          onClick={clearShoppingCar}
        >
          清空购物车
        </Button>
      </Space>
    </>
  );

  return (
    <>
      <Drawer
        title={`${tableId}号桌 购物车`}
        width={300}
        placement="right"
        onClose={onClose}
        visible={shoppingCarVisible}
        footer={operations}
      >
        {shoppingCar[0].map((dish, index) => (
          <LittleDish
            dish={dish}
            number={shoppingCar[1][index]}
            dishReduce={dishReduce}
            dishAdd={dishAdd}
          >
            {dish}
          </LittleDish>
        ))}
      </Drawer>
    </>
  );
};

export default ShoppingCar;

ShoppingCar.defaultProps = {
  tableId: 1,
  propsShoppingCarVisible: true,
  shoppingCar: [[], []],
  onClose: () => { message.error('Error'); },
  clearShoppingCar: () => {},
  dishReduce: () => {},
  dishAdd: () => {},
};

ShoppingCar.propTypes = {
  tableId: PropTypes.number,
  propsShoppingCarVisible: PropTypes.bool,
  shoppingCar: PropTypes.arrayOf(PropTypes.any),
  onClose: PropTypes.func,
  clearShoppingCar: PropTypes.func,
  dishReduce: PropTypes.func,
  dishAdd: PropTypes.func,
};
