import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import {
  Modal, Form, Input, Button, Row, Col, Tooltip,
} from 'antd';
import { CloseOutlined, PlusOutlined } from '@ant-design/icons';

const ModifyCategoryComponent = (props) => {
  const {
    categories, visible, onModify, onCancel,
  } = props;
  const [form] = Form.useForm();
  // 本地删除的菜单品种, 本地需要保留的菜单存储在form中，使用时用form.getFieldsValue().categories获取数组对象
  const [deleteCategories, setDeleteCategories] = useState([]);

  const onModifyCategoryInfo = () => {
    form
      .validateFields()
      .then((values) => {
        onModify(values.categories, deleteCategories);
      });
  };

  const onReset = () => {
    form.resetFields();
    setDeleteCategories([]);
  };

  useEffect(() => {
    onReset();
  }, [categories]);

  return (
    <Modal
      visible={visible}
      width={350}
      title="更改菜品信息"
      okText="确认"
      cancelText="取消"
      onOk={onModifyCategoryInfo}
      onCancel={() => { onCancel(); onReset(); }}
    >
      <Form
        form={form}
        name="form_in_modal"
        initialValues={{ categories }}
      >
        <Form.List
          name="categories"
        >
          {(fields, { add, remove }, { errors }) => (
            <>
              {fields.map((field) => (
                <Row style={field.name === 0 ? '' : { marginTop: '-30px' }}>
                  <Col span={20}>
                    <Form.Item
                      required={false}
                      key={field.key}
                    >
                      <Form.Item
                        name={[field.name, 'category_name']}
                        validateTrigger={['onChange', 'onBlur']}
                        rules={[
                          {
                            required: true,
                            whitespace: true,
                            message: '请输入菜品名称',
                          },
                        ]}
                        style={{ marginLeft: '8px' }}
                      >
                        <Input placeholder="请输入菜品名称" />
                      </Form.Item>
                    </Form.Item>
                  </Col>
                  <Col span={3}>
                    {fields.length > 1 ? (
                      <Tooltip
                        title="请注意，删除菜品种类后，原有的属于该菜品种类的菜品将归类为未分类菜品"
                        placement="topLeft"
                      >
                        <Button
                          type="primary"
                          danger
                          icon={<CloseOutlined />}
                          style={{ marginLeft: '10px' }}
                          // 删除菜品种类的函数，先将要删除的菜品种类加入deleteCategories中，
                          // 再在fields中调用remove方法将其删除
                          onClick={() => {
                            setDeleteCategories([...deleteCategories,
                              form.getFieldsValue().categories[field.name]]);
                            remove(field.name);
                          }}
                        />
                      </Tooltip>
                    ) : null}
                  </Col>
                </Row>
              ))}
              <Form.Item style={{ marginLeft: '7px', marginTop: '-30px' }}>
                <Button
                  type="dashed"
                  // 添加菜品种类的函数，由于是新添加的菜品，故设置其id为-1，由此判断是旧菜品还是新菜品
                  onClick={() => { add({ id: -1 }); }}
                  style={{ width: '83%' }}
                  icon={<PlusOutlined />}
                >
                  添加种类
                </Button>
                <Form.ErrorList errors={errors} />
              </Form.Item>
            </>
          )}
        </Form.List>
      </Form>
    </Modal>
  );
};

export default ModifyCategoryComponent;
ModifyCategoryComponent.defaultProps = {
  categories: [],
  visible: false,
  onModify: () => {},
  onCancel: () => {},
};

ModifyCategoryComponent.propTypes = {
  categories: PropTypes.arrayOf(PropTypes.any),
  visible: PropTypes.bool,
  onModify: PropTypes.func,
  onCancel: PropTypes.func,
};
