import React, { useEffect, useState } from 'react';
import {
  Modal, Input, Form, Col, Row, Button, Select,
} from 'antd';
import PropTypes from 'prop-types';
import UploadImage from '../UploadImage/UploadImage';
import { modifyDishImage } from '../../services/order';

const DishInfo = (props) => {
  const {
    dish, categories, type, visible, onFinish, onDelete, onCancel,
  } = props;
  const [form] = Form.useForm();
  const [image, setImage] = useState('');

  useEffect(() => {
    form.resetFields();
    setImage(dish.image);
  }, [props]);

  const onModifyDishInfo = () => {
    form
      .validateFields()
      .then((values) => {
        onFinish(values);
      });
  };

  const footerButtons = (
    <>
      {type !== 'add' ? (
        <Button
          type="primary"
          style={{ float: 'left' }}
          onClick={onDelete}
          danger
        >
          删除菜品
        </Button>
      ) : ''}
      <Button
        type="primary"
        onClick={onModifyDishInfo}
      >
        确认
      </Button>
    </>
  );

  return (
    <Modal
      visible={visible}
      title="更改菜品信息"
      onCancel={() => { onCancel(); form.resetFields(); }}
      footer={footerButtons}
    >
      {type !== 'add' ? (
        <Row justify="center">
          <Col>
            <UploadImage
              imageSrc={image}
              upLoadService={(file) => modifyDishImage(file, dish.id)}
              TooltipTitle="点击修改头像"
              callback={onModifyDishInfo}
            />
          </Col>
        </Row>
      ) : ''}
      <Form
        form={form}
        layout="vertical"
        name="form_in_modal"
        initialValues={{
          modifier: 'public',
          name: dish.name,
          price: dish.price,
          category: dish.category,
        }}
      >
        <Form.Item
          name="name"
          label="菜品名称"
          rules={[{ required: true, message: '请输入菜品名称!' }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="price"
          label="菜品单价"
          rules={[{ required: true, message: '请输入菜品单价!' }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="category"
          label="菜品种类"
          rules={[{ required: true, message: '请选择菜品种类!' }]}
        >
          <Select
            options={
              categories.map((category) => ({ label: category.category_name, value: category.id }))
            }
          />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default DishInfo;
DishInfo.defaultProps = {
  dish: '',
  categories: [],
  type: 'modify',
  visible: false,
  onFinish: () => {},
  onDelete: () => {},
  onCancel: () => {},
};

DishInfo.propTypes = {
  dish: PropTypes.objectOf(PropTypes.any),
  categories: PropTypes.arrayOf(PropTypes.any),
  type: PropTypes.string,
  visible: PropTypes.bool,
  onFinish: PropTypes.func,
  onDelete: PropTypes.func,
  onCancel: PropTypes.func,
};
