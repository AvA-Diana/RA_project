import React from 'react';
import './Footer-component.css';

const FooterComponent = () => (
  <div className="footer">
    AutoRA ©2021 Created by BugMakers
  </div>
);
export default FooterComponent;
