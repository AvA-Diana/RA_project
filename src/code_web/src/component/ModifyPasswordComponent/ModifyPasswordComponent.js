import React, { useEffect, useState } from 'react';
import {
  Modal, Button, Form, Input, message,
} from 'antd';
import { LockOutlined } from '@ant-design/icons';
import PropTypes from 'prop-types';
import axios from 'axios';

const ModifyPasswordComponent = (props) => {
  const { isModifyPasswordModalVisible, updateParent } = props;
  const [isModalVisible, setIsModalVisible] = useState(isModifyPasswordModalVisible);
  const [loading, setLoading] = useState(false);
  useEffect(() => { setIsModalVisible(isModifyPasswordModalVisible); }, [props]);
  const [formInModal] = Form.useForm();
  const handleCancel = () => {
    updateParent(false);
    setIsModalVisible(false);
    formInModal.resetFields();
  };
  const handleOk = (values) => {
    setLoading(true);
    axios.post('Account/modify_password/', {
      old_password: values.old_password,
      new_password: values.new_password,
    })
      .then((response) => {
        if (response.data === 'succeed') {
          message.success('修改成功');
          updateParent(false);
          formInModal.resetFields();
        } else if (response.data === 'wrong password') {
          message.error('密码错误，请重试');
          formInModal.resetFields();
        } else {
          message.warn('修改失败，请重试');
        }
        setLoading(false);
      }).catch(() => {
        message.warn('修改失败，请重试');
        setLoading(false);
      });
  };
  return (
    <>
      <Modal
        title="修改密码"
        visible={isModalVisible}
        onCancel={handleCancel}
        footer={[
          <Button
            key="submit"
            type="primary"
            loading={loading}
            onClick={() => {
              formInModal.validateFields()
                .then((values) => {
                  handleOk(values);
                });
            }}
          >
            提交
          </Button>,
          <Button onClick={handleCancel}>
            取消
          </Button>,
        ]}
      >
        <Form
          form={formInModal}
          name="modify_password"
          className="modify_password"
          initialValues={{
            remember: false,
          }}
        >
          <Form.Item
            name="old_password"
            rules={[
              {
                required: true,
                message: '请输入旧的密码!',
              },
            ]}
          >
            <Input
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              placeholder="旧密码"
            />
          </Form.Item>
          <Form.Item
            name="new_password"
            rules={[
              {
                required: true,
                message: '请输入新的密码!',
              },
            ]}
          >
            <Input
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              placeholder="新密码"
            />
          </Form.Item>
          <Form.Item
            name="password_check"
            dependencies={['new_password']}
            hasFeedback
            rules={[
              {
                required: true,
                message: '请再次输入密码',
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue('new_password') === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject(new Error('密码不匹配，请重新输入'));
                },
              }),
            ]}
          >
            <Input.Password
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              placeholder="再次确认新密码"
            />
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};

export default ModifyPasswordComponent;
ModifyPasswordComponent.defaultProps = {
  isModifyPasswordModalVisible: false,
  updateParent: null,
};

ModifyPasswordComponent.propTypes = {
  isModifyPasswordModalVisible: PropTypes.bool,
  updateParent: PropTypes.func,
};
