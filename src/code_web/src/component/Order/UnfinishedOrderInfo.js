import React from 'react';
import {
  Modal, Card, Button, Space, message,
} from 'antd';
import PropTypes from 'prop-types';
import { finishDishes } from '../../services/order';

const UnfinishedOrderInfo = (props) => {
  const {
    order, description, visible, onOk, onCancel, onFinish,
  } = props;

  const onSingleFinish = async () => {
    const res = await finishDishes(order.id, 1);
    if (res === 'succeed') {
      onFinish();
      message.success('成功');
    } else {
      message.error('失败，请稍后再试');
    }
  };

  const onAllFinish = async () => {
    const res = await finishDishes(order.id, order.count);
    if (res === 'succeed') {
      onFinish();
      message.success('成功');
    } else {
      message.error('失败，请稍后再试');
    }
  };

  return (
    <Modal
      className="table-info"
      visible={visible}
      okText="确定"
      cancelText="取消"
      onCancel={onCancel}
      onOk={onOk}
    >
      <Card
        bordered={false}
        style={{ textAlign: 'center', fontSize: 25 }}
      >
        <p>
          {order.dish}
        </p>
        <p>
          {description}
        </p>
        <Space>
          <Button type="primary" size="large" onClick={onSingleFinish}>完成一份</Button>
          <Button type="primary" size="large" onClick={onAllFinish}>全部完成</Button>
        </Space>
      </Card>
    </Modal>
  );
};

export default UnfinishedOrderInfo;

UnfinishedOrderInfo.defaultProps = {
  order: '',
  description: '',
  visible: false,
  onOk: () => {},
  onCancel: () => {},
  onFinish: () => {},
};

UnfinishedOrderInfo.propTypes = {
  order: PropTypes.objectOf(PropTypes.any),
  description: PropTypes.string,
  visible: PropTypes.bool,
  onOk: PropTypes.func,
  onCancel: PropTypes.func,
  onFinish: PropTypes.func,
};
