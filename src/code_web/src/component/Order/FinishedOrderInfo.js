import React from 'react';
import {
  Modal, Card, Button, Space, message,
} from 'antd';
import PropTypes from 'prop-types';
import { serveDishes } from '../../services/order';

const FinishedOrderInfo = (props) => {
  const {
    order, description, visible, onOk, onCancel, onFinish,
  } = props;

  const onServe = async () => {
    const res = await serveDishes(order.id, order.count);
    if (res === 'succeed') {
      onFinish();
      message.success('成功');
    } else {
      message.error('失败，请稍后再试');
    }
  };

  return (
    <Modal
      className="table-info"
      visible={visible}
      okText="确定"
      cancelText="取消"
      onCancel={onCancel}
      onOk={onOk}
    >
      <Card
        bordered={false}
        style={{ textAlign: 'center', fontSize: 25 }}
      >
        <p>
          {order.dish}
        </p>
        <p>
          {description}
        </p>
        <Space>
          <Button type="primary" size="large" onClick={onServe}>确认上菜</Button>
        </Space>
      </Card>
    </Modal>
  );
};

export default FinishedOrderInfo;

FinishedOrderInfo.defaultProps = {
  order: '',
  description: '',
  visible: false,
  onOk: () => {},
  onCancel: () => {},
  onFinish: () => {},
};

FinishedOrderInfo.propTypes = {
  order: PropTypes.objectOf(PropTypes.any),
  description: PropTypes.string,
  visible: PropTypes.bool,
  onOk: PropTypes.func,
  onCancel: PropTypes.func,
  onFinish: PropTypes.func,
};
