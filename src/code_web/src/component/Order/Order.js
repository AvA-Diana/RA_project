import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Card, List } from 'antd';

const { Meta } = Card;

const Order = (props) => {
  const {
    OrderInfo, order, description, onFinish,
  } = props;

  const [visible, setVisible] = useState(false);

  return (
    <>
      <List.Item
        style={{
          width: '150px',
          margin: '15px',
          boxShadow: '2px 2px 6px darkgray',
        }}
        onClick={() => { setVisible(true); }}
      >
        <Card
          cover={(
            <img
              style={{ height: '110px' }}
              alt="图片加载失败"
              src={order.image}
            />
          )}
          hoverable
          headStyle={{ fontSize: '25px' }}
        >
          <Meta
            title={order.dish}
            description={description}
          />
        </Card>
      </List.Item>
      <OrderInfo
        order={order}
        description={description}
        visible={visible}
        onOk={() => {
          setVisible(false);
        }}
        onCancel={() => {
          setVisible(false);
        }}
        onFinish={() => {
          setVisible(false);
          onFinish();
        }}
      />
    </>
  );
};

export default Order;
Order.defaultProps = {
  OrderInfo: () => {},
  order: '',
  description: '',
  onFinish: () => {},
};

Order.propTypes = {
  OrderInfo: PropTypes.func,
  order: PropTypes.objectOf(PropTypes.any),
  description: PropTypes.string,
  onFinish: PropTypes.func,
};
