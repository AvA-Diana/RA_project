import React from 'react';
import {
  Form, Input, Button, message,
} from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import './LoginBox.css';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { login, setCookie } from '../../services/account';

const LoginBox = (props) => {
  const onFinish = async (values) => {
    const res = await login(values);
    if (res === 'succeed') {
      message.success('登录成功');
      setCookie('login', true, 15);
      props.history.push('/home');
    } else {
      message.error('密码错误，请重新再试');
    }
  };

  return (
    <Form
      name="normal_login"
      className="login-form"
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
    >
      <Form.Item
        name="username"
        rules={[
          {
            required: true,
            message: '请输入您的邮箱!',
          },
        ]}
      >
        <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="邮箱" />
      </Form.Item>
      <Form.Item
        name="password"
        rules={[
          {
            required: true,
            message: '请输入您的密码!',
          },
        ]}
      >
        <Input
          prefix={<LockOutlined className="site-form-item-icon" />}
          type="password"
          placeholder="密码"
        />
      </Form.Item>
      <Form.Item>
        <Button type="link" className="login-form-forgot" href="./forgetPassword">
          忘记密码
        </Button>
      </Form.Item>

      <Form.Item>
        <Button type="primary" htmlType="submit" className="login-form-button">
          登录
        </Button>
        {' '}
        或者
        <Button type="link" href="./register">立刻注册</Button>
      </Form.Item>
    </Form>
  );
};

export default withRouter(LoginBox);
LoginBox.defaultProps = {
  history: {},
};

LoginBox.propTypes = {
  history: PropTypes.objectOf(PropTypes.any),
};
