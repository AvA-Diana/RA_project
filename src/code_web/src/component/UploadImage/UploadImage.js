import React from 'react';
import {
  Upload, message, Tooltip,
} from 'antd';
import PropTypes from 'prop-types';
import { PlusOutlined } from '@ant-design/icons';

const UploadImage = (props) => {
  const { imageSrc, TooltipTitle } = props;

  const beforeUpload = (file) => {
    const isFileTypeValid = ['image/jpeg', 'image/png', 'image/gif'].includes(file.type);
    if (!isFileTypeValid) {
      message.error('只支持上传 jpg / png / gif 格式的图片');
      return false;
    }
    const isLt1M = file.size / 1024 / 1024 < 1;
    if (!isLt1M) {
      message.error('图片大小不能超过 1M');
      return false;
    }
    return true;
  };
  const doCustomUpload = async (info) => {
    const { file } = info;
    const res = await props.upLoadService(file);
    if (res === 'succeed') {
      message.success('上传成功');
      props.callback();
    } else {
      message.error('上传失败');
    }
  };

  const uploadButton = (
    <div>
      <PlusOutlined />
      <div style={{ marginTop: 8 }}>Upload</div>
    </div>
  );

  return (
    <>
      <Tooltip title={TooltipTitle}>
        <Upload
          name="image"
          listType="picture-card"
          className="image-uploader"
          showUploadList={false}
          beforeUpload={beforeUpload}
          customRequest={doCustomUpload}
        >
          {imageSrc ? <img src={imageSrc} alt="myImage" style={{ width: '100%' }} /> : uploadButton}
        </Upload>
      </Tooltip>
    </>
  );
};

export default UploadImage;
UploadImage.defaultProps = {
  imageSrc: '',
  upLoadService: () => {},
  TooltipTitle: '点击更换',
  callback: () => {},
};

UploadImage.propTypes = {
  imageSrc: PropTypes.string,
  upLoadService: PropTypes.func,
  TooltipTitle: PropTypes.string,
  callback: PropTypes.func,
};
