import React, { useState } from 'react';
import {
  Form, Input, Button, message,
} from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import MyButton from '../MyButton/MyButton';
import { sendVerificationCode, resetPassword } from '../../services/account';

const ForgetPasswordBox = (props) => {
  const [resetDisabled, setResetDisabled] = useState(true);
  const [sendVerificationCodeDisabled, setSendVerificationCodeDisabled] = useState(true);
  const [form] = Form.useForm();
  const onFinish = async (values) => {
    const res = await resetPassword(values.username, values.verificationCode, values.password);
    if (res === 'succeed') {
      message.success('重置密码成功');
      props.history.push('/login');
    } else if (res === 'verification failure') {
      message.error('验证码错误');
    } else {
      message.error('重置失败,请重试');
    }
  };
  const handleSendVerificationCode = async () => {
    setResetDisabled(true);
    const res = await sendVerificationCode(form.getFieldValue('username'));
    if (res !== 'succeed') {
      message.error('发送验证码失败，请重新再试');
    } else {
      message.success('发送成功，请检查您的邮箱');
    }
  };
  const handleFormValueChange = () => {
    const email = form.getFieldValue('username');
    if (/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(email)) {
      setSendVerificationCodeDisabled(false);
    } else {
      setSendVerificationCodeDisabled(true);
    }
  };

  return (
    <Form
      name="normal_forget_password"
      className="forget_password-form"
      form={form}
      onValuesChange={handleFormValueChange}
      onFinish={onFinish}
    >
      <Form.Item
        name="username"
        hasFeedback
        rules={[
          {
            required: true,
            message: '请输入您的邮箱!',
          },
          {
            pattern: new RegExp(/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/),
            message: '请输入有效的邮箱地址',
          },
        ]}
      >
        <Input
          prefix={<UserOutlined className="site-form-item-icon" />}
          placeholder="邮箱"
        />
      </Form.Item>
      <Form.Item style={{ marginBottom: 0 }}>
        <Input.Group compact>
          <Form.Item
            name="verificationCode"
            style={{ display: 'inline-block', width: 'calc(100% - 150px)' }}
            rules={[
              {
                required: true,
                message: '请输入验证码!',
              },
            ]}
          >
            <Input
              prefix={<LockOutlined className="site-form-item-icon" />}
              placeholder="验证码"
            />
          </Form.Item>
          <Form.Item>
            <MyButton
              maxCount={30}
              onClick={handleSendVerificationCode}
              buttonDisabled={sendVerificationCodeDisabled}
            />
          </Form.Item>
        </Input.Group>
      </Form.Item>
      <Form.Item
        name="password"
        rules={[{
          required: true,
          message: '请输入新密码',
        }]}
        hasFeedback
      >
        <Input.Password
          prefix={<LockOutlined className="site-form-item-icon" />}
          placeholder="密码"
        />
      </Form.Item>
      <Form.Item
        name="password_check"
        dependencies={['password']}
        hasFeedback
        rules={[
          {
            required: true,
            message: '请再次输入密码',
          },
          ({ getFieldValue }) => ({
            validator(_, value) {
              if (!value || getFieldValue('password') === value) {
                return Promise.resolve();
              }
              return Promise.reject(new Error('密码不匹配，请重新输入'));
            },
          }),
        ]}
      >
        <Input.Password
          prefix={<LockOutlined className="site-form-item-icon" />}
          type="password"
          placeholder="再次确认密码"
        />
      </Form.Item>

      <Form.Item>
        <Button type="primary" htmlType="submit" className="login-form-button" disabled={resetDisabled}>
          重置密码
        </Button>
      </Form.Item>
      <Form.Item>
        <Button type="link" href="./login">返回登录界面</Button>
      </Form.Item>
    </Form>
  );
};

export default withRouter(ForgetPasswordBox);
ForgetPasswordBox.defaultProps = {
  history: {},
};

ForgetPasswordBox.propTypes = {
  history: PropTypes.objectOf(PropTypes.any),
};
