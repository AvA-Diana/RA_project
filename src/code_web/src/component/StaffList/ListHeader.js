import React, { useState } from 'react';
import './ListHeader.css';
import { Button, message } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import PropTypes from 'prop-types';
import NewStaff from './NewStaff';
import { register } from '../../services/account';

const ListHeader = (props) => {
  const [visible, setVisible] = useState(false);
  const { type, onReload } = props;
  let chineseType;
  switch (type) {
    case 'manager':
      chineseType = '经理';
      break;
    case 'waiter':
      chineseType = '服务员';
      break;
    case 'cook':
      chineseType = '厨师';
      break;
    case 'busboy':
      chineseType = '清洁工';
      break;
    case 'receptionist':
      chineseType = '前台';
      break;
    default:
      chineseType = '职位';
      break;
  }

  const onCreate = async (values) => {
    const registerValues = values;
    registerValues.role_type = type;
    const res = await register(registerValues);
    if (res === 'succeed') {
      message.success('添加员工成功');
    } else if (res === 'Duplicate username') {
      message.error('该邮箱不可用，请更换');
    } else {
      message.error('注册失败，请重新再试');
    }
    setVisible(false);
    onReload();
  };

  return (
    <header className="header">
      { chineseType }
      <Button
        type="primary"
        className="add-button"
        size="small"
        icon={<PlusOutlined />}
        onClick={() => {
          setVisible(true);
        }}
      />
      <NewStaff
        visible={visible}
        onCreate={onCreate}
        onCancel={() => {
          setVisible(false);
        }}
      />
    </header>
  );
};

export default ListHeader;

ListHeader.defaultProps = {
  type: 'roleType',
  onReload: () => {},
};

ListHeader.propTypes = {
  type: PropTypes.string,
  onReload: PropTypes.func,
};
