import React, { useState, useEffect } from 'react';
import {
  List, Avatar, message, Divider,
} from 'antd';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import ListHeader from './ListHeader';
import './StaffList.css';
import { getStaffList } from '../../services/staff';

const StaffList = (props) => {
  const [staffs, setStaffs] = useState([]);
  const { type, history } = props;

  const loadData = async () => {
    const res = await getStaffList(type);
    if (res !== 'failed') {
      setStaffs(res);
    } else {
      message.error('获取用户信息失败，请重新再试');
    }
  };

  useEffect(() => {
    loadData();
  }, []);

  return (
    <div
      className="staff-list"
    >
      <ListHeader type={type} onReload={loadData} />
      <List
        dataSource={staffs}
        renderItem={(staff) => (
          <List.Item
            className="staff-item"
            key={staff.id}
            onClick={() => history.push(`/home/manageStaff/${staff.id}`)}
          >
            <List.Item.Meta
              avatar={<Avatar src={staff.avatar} />}
              title={<p>{staff.name}</p>}
            />
          </List.Item>
        )}
      />
      <Divider id="footer" hidden={staffs.length === 0} plain>end</Divider>
    </div>
  );
};

export default withRouter(StaffList);

StaffList.defaultProps = {
  type: 'roleType',
  history: '',
};

StaffList.propTypes = {
  type: PropTypes.string,
  history: PropTypes.objectOf(PropTypes.any),
};
