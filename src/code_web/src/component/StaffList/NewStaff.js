import React from 'react';
import {
  Modal, Form, Input, Select,
} from 'antd';
import PropTypes from 'prop-types';
import {
  LockOutlined, MailOutlined, PhoneOutlined, UserOutlined,
} from '@ant-design/icons';

const NewStaff = (props) => {
  const { visible, onCreate, onCancel } = props;
  const [form] = Form.useForm();
  return (
    <Modal
      visible={visible}
      title="增加新员工"
      okText="确定"
      cancelText="取消"
      onCancel={onCancel}
      onOk={() => {
        form
          .validateFields()
          .then((values) => {
            onCreate(values);
          });
      }}
    >
      <Form
        form={form}
        layout="vertical"
        name="form_in_modal"
        initialValues={{
          modifier: 'public',
        }}
      >
        <Form.Item
          name="name"
          rules={[{ required: true }]}
        >
          <Input
            prefix={<UserOutlined className="site-form-item-icon" />}
            placeholder="姓名"
          />
        </Form.Item>
        <Form.Item name="sex_type">
          <Select placeholder="性别">
            <Select.Option value="male">男</Select.Option>
            <Select.Option value="female">女</Select.Option>
          </Select>
        </Form.Item>
        <Form.Item
          name="email"
          hasFeedback
          rules={[
            {
              required: true,
              message: '请输入你的电子邮箱',
            },
            {
              pattern: new RegExp(/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/),
              message: '请输入有效的邮箱地址',
            },
          ]}
        >
          <Input
            prefix={<MailOutlined className="site-form-item-icon" />}
            placeholder="电子邮箱"
          />
        </Form.Item>
        <Form.Item
          name="phone"
          hasFeedback
          rules={[
            {
              required: true,
              message: '请输入你的电话号码',
            },
            {
              pattern: new RegExp(/^(13[0-9]|14[5|7]|15[0-9]|18[0-9])\d{8}$/),
              message: '请输入有效电话号码',
            },
          ]}
        >
          <Input
            prefix={<PhoneOutlined className="site-form-item-icon" />}
            placeholder="电话号码"
          />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[{
            required: true,
            message: '请输入密码',
          }]}
          hasFeedback
        >
          <Input.Password
            prefix={<LockOutlined className="site-form-item-icon" />}
            placeholder="密码"
          />
        </Form.Item>
        <Form.Item
          name="password_check"
          dependencies={['password']}
          hasFeedback
          rules={[
            {
              required: true,
              message: '请再次输入密码',
            },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue('password') === value) {
                  return Promise.resolve();
                }
                return Promise.reject(new Error('密码不匹配，请重新输入'));
              },
            }),
          ]}
        >
          <Input.Password
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="再次确认密码"
          />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default NewStaff;

NewStaff.defaultProps = {
  visible: false,
  onCreate: () => {},
  onCancel: () => {},
};

NewStaff.propTypes = {
  visible: PropTypes.bool,
  onCreate: PropTypes.func,
  onCancel: PropTypes.func,
};
