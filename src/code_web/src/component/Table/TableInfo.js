import React, { useEffect, useState } from 'react';
import {
  Modal, Card, Button, Space, Input, message,
} from 'antd';
import PropTypes from 'prop-types';
import './TableInfo.css';
import { withRouter } from 'react-router-dom';
import { getFormatDate } from '../../services/public';
import { toOccupyTable, toReleaseTable, toCleanTable } from '../../services/table';

const TableInfo = (props) => {
  const {
    table, visible, onCancel, history,
  } = props;
  const [tableState, setTableState] = useState('');
  const [peopleCount, setPeopleCount] = useState(1);

  useEffect(() => {
    if (table.state === 0) {
      setTableState('空闲中');
    } else if (table.state === 1) {
      setTableState('待请理');
    } else if (table.state === 2) {
      setTableState('用餐中');
    }
  }, [props]);

  const occupyTable = async () => {
    const num = Number(peopleCount);
    if (Number.isNaN(num)) {
      message.error('请输入数字');
      return;
    }
    if (num < 1 || num > 100) {
      message.error('请输入正确就餐人数');
      return;
    }
    const res = await toOccupyTable(table.number, getFormatDate(), peopleCount);
    if (res === 'succeed') {
      message.success('占用成功');
      props.occupyTableCallBack();
      onCancel();
    } else {
      message.error('占用失败，请重试');
    }
  };

  const releaseTable = async () => {
    const res = await toReleaseTable(table.number, getFormatDate());
    if (res === 'succeed') {
      message.success('结束成功');
      props.releaseTableCallBack();
      onCancel();
    } else {
      message.error('结束失败，请重试');
    }
  };

  const cleanTable = async () => {
    const res = await toCleanTable(table.number);
    if (res === 'succeed') {
      message.success('清洁成功');
      props.cleanTableCallBack();
      onCancel();
    } else {
      message.error('清洁失败，请重试');
    }
  };

  const renderButton = (state) => {
    switch (state) {
      case 0:
        return (
          <>
            <Space direction="vertical">
              <Input addonBefore="就餐人数" defaultValue={1} onChange={(e) => setPeopleCount(e.target.value)} />
              <Button type="primary" size="default" onClick={occupyTable}>占用该桌</Button>
            </Space>
          </>
        );
      case 1:
        return <Button type="primary" size="large" onClick={cleanTable}>清理完毕</Button>;
      case 2:
        return (
          <>
            <Space>
              <Button type="primary" size="large" onClick={() => history.push(`/home/orderDish/${table.number}`)}>前往点餐</Button>
              <Button type="danger" size="large" onClick={releaseTable}>结束用餐</Button>
            </Space>
          </>
        );
      default:
        return 'grown';
    }
  };

  return (
    <Modal
      className="table-info"
      visible={visible}
      onCancel={onCancel}
      footer={null}
    >
      <Card title={`${table.number}号桌`} bordered={false} style={{ textAlign: 'center', fontSize: 25 }}>
        <p>
          状态:
          {' '}
          {tableState}
        </p>
        {
          renderButton(table.state)
        }
      </Card>
    </Modal>
  );
};

export default withRouter(TableInfo);

TableInfo.defaultProps = {
  table: '',
  visible: false,
  onCancel: () => {},
  history: '',
  occupyTableCallBack: () => {},
  releaseTableCallBack: () => {},
  cleanTableCallBack: () => {},
};

TableInfo.propTypes = {
  table: PropTypes.objectOf(PropTypes.any),
  visible: PropTypes.bool,
  onCancel: PropTypes.func,
  history: PropTypes.objectOf(PropTypes.any),
  occupyTableCallBack: PropTypes.func,
  releaseTableCallBack: PropTypes.func,
  cleanTableCallBack: PropTypes.func,
};
