import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Draggable from 'react-draggable';
import { message } from 'antd';
import TableInfo from './TableInfo';
import { moveTable } from '../../services/table';
import tableStatusEnum from '../../constant/tableSatus';
import './Table.css';

const Table = (props) => {
  const {
    table, draggable,
  } = props;
  const [deltaPosition, setDeltaPosition] = useState({ x: 0, y: 0 });
  const [visible, setVisible] = useState(false);

  const handleStart = () => {
  };
  const handleDrag = (e, ui) => {
    const { x, y } = deltaPosition;
    setDeltaPosition({ x: x + ui.deltaX, y: y + ui.deltaY });
  };

  const handleStop = async () => {
    const { x, y } = deltaPosition;
    const res = await moveTable(table.id, table.x + x, table.y + y);
    if (res === 'failed') {
      message.error('桌子位置信息保存出错，请重新再试');
    }
  };
  const getTableColor = (state) => {
    switch (state) {
      case tableStatusEnum.AVAILABLE:
        return '#7ec343';
      case tableStatusEnum.TO_BE_CLEANED:
        return 'red';
      case tableStatusEnum.IN_USE:
        return 'yellow';
      default:
        return 'grown';
    }
  };
  const occupyTableCallBack = () => {
    table.state = tableStatusEnum.IN_USE;
  };
  const releaseTableCallBack = () => {
    table.state = tableStatusEnum.TO_BE_CLEANED;
  };
  const cleanTableCallBack = () => {
    table.state = tableStatusEnum.AVAILABLE;
  };
  return (
    draggable
      ? (
        <Draggable
          defaultPosition={{ x: table.x, y: table.y }}
          grid={[25, 25]}
          scale={1}
          onStart={handleStart}
          onDrag={handleDrag}
          onStop={handleStop}
          bounds="parent"
        >
          <div
            className="table"
            tabIndex={table.number}
            role="button"
            style={{
              backgroundColor: 'brown',
              cursor: 'move',
            }}
          >
            {table.number}
            号桌
          </div>
        </Draggable>
      )
      : (
        <>
          <div
            className="table"
            tabIndex={0}
            role="button"
            style={{
              backgroundColor: getTableColor(table.state),
              left: table.x,
              top: table.y,
              color: 'black',
              cursor: 'pointer',
            }}
            onClick={() => { setVisible(true); }}
            onKeyDown={() => {}}
          >
            {table.number}
            号桌
          </div>
          <TableInfo
            table={table}
            visible={visible}
            onCreate={() => {
              setVisible(false);
            }}
            onCancel={() => {
              setVisible(false);
            }}
            occupyTableCallBack={occupyTableCallBack}
            releaseTableCallBack={releaseTableCallBack}
            cleanTableCallBack={cleanTableCallBack}
          />
        </>
      )
  );
};

export default Table;
Table.defaultProps = {
  table: {},
  draggable: true,
};

Table.propTypes = {
  table: PropTypes.objectOf(PropTypes.any),
  draggable: PropTypes.bool,
};
