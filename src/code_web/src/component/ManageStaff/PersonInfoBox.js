import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import {
  Button, Descriptions, Input, message, Popconfirm, Select,
} from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import { resetAccountInfo } from '../../services/account';

const PersonInfoBox = (props) => {
  const { data, id } = props;

  const [localData, setLocalData] = useState('');
  const [edit, setEdit] = useState(false);

  useEffect(() => {
    setLocalData(data);
  }, [props]);

  const onEdit = () => {
    setEdit(true);
  };

  const onSubmit = async () => {
    let patt = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
    if (!patt.test(localData.email)) {
      message.error('请输入正确的邮箱!');
      return;
    }
    patt = /^(17[0-9]|13[0-9]|14[5|7]|15[0-9]|18[0-9])\d{8}$/;
    if (!patt.test(localData.phone)) {
      message.error('请输入正确的手机号码!');
      return;
    }
    const res = await resetAccountInfo(id, localData);
    if (res === 'succeed') {
      setEdit(false);
      message.success('修改成功!');
    } else if (res === 'Duplicate email') {
      message.error('该邮箱地址已存在，请重新尝试！');
    } else {
      message.error('修改用户信息失败，请重新尝试！');
    }
  };

  return (
    <Descriptions
      title={data.role}
      className="information"
      extra={(
        <div>
          <Button
            type="primary"
            className="edit-info-button"
            onClick={onEdit}
          >
            编辑
          </Button>
          <Popconfirm
            title="确认提交信息"
            onConfirm={onSubmit}
            okText="确认"
            cancelText="取消"
            icon={<UploadOutlined />}
            disabled={!edit}
          >
            <Button
              type="primary"
              className="edit-info-button"
              disabled={!edit}
            >
              提交
            </Button>
          </Popconfirm>
        </div>
          )}
    >
      <Descriptions.Item label="姓名" labelStyle={{ marginTop: 4 }}>
        {edit ? (
          <Input
            id="name"
            defaultValue={localData.name}
            style={{ paddingLeft: 2 }}
            onChange={(e) => {
              const changes = localData;
              changes.name = e.target.value;
              setLocalData(changes);
            }}
          />
        )
          : <p style={{ marginTop: 4 }}>{localData.name}</p>}
      </Descriptions.Item>
      <Descriptions.Item label="性别" labelStyle={{ marginTop: 4, marginLeft: 100 }} span={2}>
        {edit ? (
          <Select
            key={localData.sex}
            defaultValue={localData.sex}
            bordered={edit}
            showArrow={false}
            onChange={(e) => {
              const changes = localData;
              changes.sex = e;
              setLocalData(changes);
            }}
          >
            <Select.Option value="male">男</Select.Option>
            <Select.Option value="female">女</Select.Option>
          </Select>
        )
          : <p style={{ marginTop: 4 }}>{data.sex}</p>}
      </Descriptions.Item>
      <Descriptions.Item label="邮箱" labelStyle={{ marginTop: 4 }}>
        {edit ? (
          <Input
            id="email"
            defaultValue={localData.email}
            bordered={edit}
            style={{ paddingLeft: 2 }}
            onChange={(e) => {
              const changes = localData;
              changes.email = e.target.value;
              changes.username = e.target.value;
              setLocalData(changes);
            }}
          />
        )
          : <p style={{ marginTop: 4 }}>{localData.email}</p>}
      </Descriptions.Item>
      <Descriptions.Item label="电话" labelStyle={{ marginTop: 4, marginLeft: 100 }}>
        {edit ? (
          <Input
            id="phone"
            defaultValue={localData.phone}
            bordered={edit}
            style={{ paddingLeft: 2 }}
            onChange={(e) => {
              const changes = localData;
              changes.phone = e.target.value;
              setLocalData(changes);
            }}
          />
        )
          : <p style={{ marginTop: 4 }}>{localData.phone}</p>}
      </Descriptions.Item>
    </Descriptions>
  );
};

export default PersonInfoBox;

PersonInfoBox.defaultProps = {
  data: '',
  id: 1,
};

PersonInfoBox.propTypes = {
  data: PropTypes.objectOf(PropTypes.any),
  id: PropTypes.number,
};
