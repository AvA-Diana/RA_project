import React from 'react';
import { Space } from 'antd';
import StaffList from '../StaffList/StaffList';
import './OverView.css';

const OverView = () => (
  <Space
    className="site-card-wrapper"
    size="large"
  >
    <StaffList type="manager" />
    <StaffList type="waiter" />
    <StaffList type="cook" />
    <StaffList type="receptionist" />
    <StaffList type="busboy" />
  </Space>
);

export default OverView;
