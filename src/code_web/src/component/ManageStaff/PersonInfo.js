import React, { useEffect, useState } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import {
  message, Card, Avatar, Button, Popconfirm,
} from 'antd';
import PersonInfoBox from './PersonInfoBox';
import { getStaff } from '../../services/staff';
import './PersonInfo.css';
import { deleteAccount } from '../../services/account';

const PersonInfo = (props) => {
  const { match, history } = props;
  const [data, setData] = useState('');
  const { Meta } = Card;

  const loadData = async () => {
    const res = await getStaff(match.params.id);
    if (res !== 'failed') {
      setData(res);
    } else {
      message.error('获取用户信息失败，请重新再试');
    }
  };

  const onDelete = async () => {
    const res = await deleteAccount(match.params.id);
    if (res === 'succeed') {
      message.success('删除成功!');
      history.goBack();
    } else {
      message.error('删除失败，请稍后再试！');
    }
  };

  useEffect(() => {
    loadData();
  }, []);

  const title = (
    <Button
      type="primary"
      className="return-button"
      onClick={history.goBack}
    >
      返回
    </Button>
  );
  const extra = (
    <Popconfirm
      title="确认删除该员工?"
      onConfirm={onDelete}
      okText="确认"
      cancelText="取消"
    >
      <Button
        type="primary"
        className="button"
        danger
      >
        删除员工
      </Button>
    </Popconfirm>
  );
  return (
    <Card
      id="info-card"
      title={title}
      extra={extra}
    >
      <Meta
        avatar={<Avatar src={data.avatar} id="avatar" />}
      />
      <PersonInfoBox data={data} id={match.params.id} />
    </Card>
  );
};

export default withRouter(PersonInfo);

PersonInfo.defaultProps = {
  match: '',
  history: '',
};

PersonInfo.propTypes = {
  match: PropTypes.objectOf(PropTypes.any),
  history: PropTypes.objectOf(PropTypes.any),
};
