import React, { useEffect, useState } from 'react';
import { Button } from 'antd';
import PropTypes from 'prop-types';

const MyButton = (props) => {
  const { buttonDisabled } = props;
  const [msg, setMsg] = useState('发送验证码');
  const [disabled, setDisabled] = useState(buttonDisabled);

  useEffect(() => {
    setDisabled(buttonDisabled);
  }, [props]);

  const m = '秒后可再次发送';
  const handleClick = () => {
    props.onClick();
    let count = props.maxCount;
    setMsg(count + m);
    setDisabled(true);
    const id = setInterval(() => {
      if (count > 0) {
        count -= 1;
        setMsg(count + m);
      } else {
        clearInterval(id);
        setMsg('没收到?再次发送');
        setDisabled(false);
      }
    }, 1000);
  };
  return (
    <Button
      style={{ width: 150 }}
      onClick={handleClick}
      disabled={disabled}
    >
      {msg}
    </Button>
  );
};

export default MyButton;
MyButton.defaultProps = {
  maxCount: 10,
  onClick: () => {},
  buttonDisabled: false,
};

MyButton.propTypes = {
  maxCount: PropTypes.number,
  onClick: PropTypes.func,
  buttonDisabled: PropTypes.bool,
};
