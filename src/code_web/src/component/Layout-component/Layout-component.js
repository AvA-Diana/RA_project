import React from 'react';
import { Layout, Card } from 'antd';
import { Footer, Header } from 'antd/es/layout/layout';
import PropTypes from 'prop-types';
import imgURL from '../../assets/images/logo.png';
import './Layout-component.css';
import FooterComponent from '../Footer-component/Footer-component';

const { Content } = Layout;
const LayoutComponent = (props) => {
  const { Box, title } = props;
  return (
    <Layout
      className="layout"
    >
      <Header>
        <div>
          <img src={imgURL} alt="logo" height="55px" width="140px" />
        </div>
      </Header>
      <Content
        className="layout-content"
      >
        <Card
          title={title}
          headStyle={{ textAlign: 'center' }}
        >
          <Box />
        </Card>
      </Content>
      <Footer className="layout-footer">
        <FooterComponent />
      </Footer>
    </Layout>
  );
};

export default LayoutComponent;

LayoutComponent.defaultProps = {
  Box: <div />,
  title: ' ',
};

LayoutComponent.propTypes = {
  Box: PropTypes.func,
  title: PropTypes.string,
};
