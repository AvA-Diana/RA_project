import React, { useEffect } from 'react';
import './LittleDish.css';
import { Card, Avatar } from 'antd';
import PropTypes from 'prop-types';
import { MinusCircleTwoTone, PlusCircleTwoTone } from '@ant-design/icons';

const { Meta } = Card;

const LittleDish = (props) => {
  const {
    dish, number, haveActions, dishReduce, dishAdd,
  } = props;
  useEffect(() => {

  }, [props]);
  const url = `https://www.bugmakers.top${dish.image}`;
  const actions = haveActions
    ? [
      <MinusCircleTwoTone onClick={() => dishReduce(dish)} />,
      `×${number}`,
      <PlusCircleTwoTone onClick={() => dishAdd(dish)} />,
    ] : [
      `×${number}`,
    ];

  return (
    <Card
      className="little-dish"
      actions={actions}
    >
      <Meta
        avatar={(
          <Avatar
            alt="img"
            shape="square"
            size={50}
            src={url}
          />
        )}
        title={dish.name}
        description={`单价:${dish.price}元`}
      />
    </Card>
  );
};

export default LittleDish;

LittleDish.defaultProps = {
  dish: true,
  number: 1,
  haveActions: true,
  dishReduce: () => {},
  dishAdd: () => {},
};

LittleDish.propTypes = {
  dish: PropTypes.objectOf(PropTypes.any),
  number: PropTypes.number,
  haveActions: PropTypes.bool,
  dishReduce: PropTypes.func,
  dishAdd: PropTypes.func,
};
