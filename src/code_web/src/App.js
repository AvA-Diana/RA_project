import React from 'react';
import './App.css';
import axios from 'axios';
import { Redirect, Route, Switch } from 'react-router-dom';
import LoginPage from './pages/LoginPage/LoginPage';
import RegisterPage from './pages/RegisterPage/RegisterPage';
import PrimaryPage from './pages/HomePages/PrimaryPage/PrimaryPage';
import PrivateRoute from './component/PrivateRoute/PrivateRoute';
import ForgetPasswordPage from './pages/ForgetPasswordPage/ForgetPasswordPage';

axios.defaults.withCredentials = true;
axios.defaults.xsrfHeaderName = 'X-CSRFTOKEN';
axios.defaults.xsrfCookieName = 'csrftoken';
axios.defaults.baseURL = 'https://www.bugmakers.top/demo/';
// axios.defaults.baseURL = 'http://localhost:3200/demo/';
axios.defaults.timeout = 3000;

const App = () => (
  <Switch>
    <Route
      path="/login"
      component={LoginPage}
    />
    {' '}
    <Route
      path="/register"
      component={RegisterPage}
    />
    {' '}
    <Route
      path="/forgetPassword"
      component={ForgetPasswordPage}
    />
    {' '}
    <PrivateRoute
      path="/home"
      component={PrimaryPage}
    />
    {' '}
    <Redirect
      path="*"
      to={
        { pathname: '/login' }
}
    />
    {' '}

  </Switch>
);

export default App;
