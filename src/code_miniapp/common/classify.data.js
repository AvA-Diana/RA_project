export default[
    {
       "name": "主食",
       "foods": [
           {
             "name": "牛排",
             "key": "牛排",
             "price": "80.00",
             "icon": "https://p.inari.site/home/a/211103/PknK-牛排.jfif",
             "cat": 10
           },
           {
             "name": "烤鸡",
             "key": "烤鸡",
             "price": "100.00",
             "icon": "https://p.inari.site/home/a/211103/nkG3-烤鸡.jfif",
             "cat": 10
           }
       ]
    },
    {
     "name": "甜品",
     "foods": [
         {
           "name": "蛋糕",
           "key": "蛋糕",
           "price": "40.00",
           "icon": "https://p.inari.site/home/a/211103/Bnc1-蛋糕.jfif",
           "cat": 6
         },
         {
           "name": "糕点饼干",
           "key": "糕点饼干",
           "price": "20.00",
           "icon": "https://p.inari.site/home/a/211103/euIQ-糕点饼干.jfif",
           "cat": 6
         }
         ]
     }
]