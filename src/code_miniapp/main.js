import App from './App'
import uView from "uview-ui";
// #ifndef VUE3
import Vue from 'vue'
Vue.use(uView);
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
    ...App
})
app.$mount()
// #endif
//const BASE_URL = 'http://localhost:3200/demo/';
const BASE_URL = 'https://www.bugmakers.top/demo/';

Vue.prototype.$sendRequest = (options) => {
  uni.request({
    url: BASE_URL + options.url,
    method: options.method || 'GET',
    data: options.data || {},
		timeout: 5000,
    success: (response) => {
      options.success(response)
    },
    fail: (error) => {
      uni.showToast({
        title: "请求接口失败"
      })
      reject(error)
    }
  })
}
// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif