部署环境方式：

1. 拉取代码

2. 进入目录/src/code_web下执行命令

   ```shell
   npm install
   ```

3. 启动docker

4. 回到根目录处执行以下命令

   ```shell
   docker compose build
   docker compose up -d
   ```

5. 进入数据库容器（mariadb）内执行以下命令
	```shell
	mysql -u root -p
	RA-docker
	```
	```sql
	GRANT ALL PRIVILEGES ON *.* TO 'root'@'%'IDENTIFIED BY 'RA-docker' WITH GRANT OPTION;
	flush privileges;
	```

6. 重启后端（backend）容器

7. 进入后端（backend）容器执行以下命令

   ```shell
   python manage.py makemigrations
   python manage.py migrate
   ```

